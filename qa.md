_“The game server is state based, meaning it does not use a database to avoid bottlenecks in performance”_

#### What if something will break? The node will go down?
How the player state will be restored? Is there any mechanism to allow HA and avoiding SPOF?

see "persistence" section of Game Server document. The game server has a reliable persistence mechanism, but is file based - not a relational DB
By default, the GS will persist a sub-set of crucial data, such as money a player brought to a cash game or tournament ticket reservations. Actual poker game state is
not stored, so games are not resurrected in the event of a Game Server crash, but money held is guaranteed.

If running clustered, the GS does not have a SPOF, although if a node goes down all running poker games will be interrupted and any money must be refunded. Although not supported right now, failover for running games is a tractable problem because we can "switch on" backup for the poker games themselves. There would be some development effort to manage the seamless transition for the players from old to new node, and so far the investment has not been worth it for us. But we are in better shape than most for tackling this problem

Although we only run a single node for our current HandsOfVictory deployment, we can spin up new GS nodes relatively easily to increase our availability. Rolling upgrades will be straight forward for HOV by phasing out old nodes (prevent new games starting, let running games finish)

#### What is the responsibility of Mysql database and how u keep data consistent between state based and mysql? What are the patterns being used to make data consistent?

I hope the Game Server tech document gives a sense of how the GS is decoupled from the "platform" (which runs a MySQL DB). With Hands Of Victory, we are lucky that we don't face any data consistency problems. The platform simply sends players to the GS, and the GS reports back when a game actually starts and ends. The platform _does_ track ongoing player sessions, which can potentially get out of sync if, say you killed the GS while a game is running. The impact if this is minimal however, because the web-app actually queries the GS directly for the use-case whereby a player, say, kills and reopens their browser - they will be brought directly back to their game.

#### What are non-functional parameters related with each component?

Game Server: 
when it comes to performance, Hands Of Victory has not been stressed "in the field" because of lack of players. Here are some points about performance:
1. I've performed stress tests on my laptop with 1000 concurrent bots playing Hands Of Victory games without any issues. We would like to do larger more conclusive tests, but we've lacked the infrastructure to do so
1. The game server has been designed and built by an Ongame veteran who was an integral part of architecting their game server, which has been proven in the field. In other words, there's no reason why this GS should not scale at least as well as theirs

**robustness**: the game server can run happily for months without memory leaks or crashes. Note we have lacked large player volumes with Hands Of Victory, but the GS has held up very well so far

**extensibility**: this is a big USP with the game server - see GS document

again, see GS doc for more detail on the non-functional aspects


#### What are hardware parameters for each of the platform component?
Dan?