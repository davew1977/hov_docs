# Software Docs

## Game Server
### Introduction
The game server (GS) is a Java Application dedicated to running poker games, tournaments and lobbies. It is decoupled from any gaming platform, meaning it can easily be plugged in to an existing one. The following APIs detail its inputs and outputs:

+ Game Clients (Game, Lobby, Lounge etc) _Websockets (public)_
+ GS `<->` Platform _REST (private)_ - used for obtaining player information, reporting game events (e.g start, elimination, game end), hand history etc
+ Platform `<->` GS _REST (private)_ - used for admin and system monitoring

The game server started out as a full-featured poker engine with cash-games, SNG and tournament support. Hands-of-victory was created later on and every care has been taken to keep the core poker engine decoupled from this "extension".

##### Technical Highlights
+ The GS is a modern Java/Spring app which leverages [Hazelcast](https://hazelcast.org) for multi-node communication
+ Simple deployment - runs with very little configuration and no database dependencies
+ Apis and core types are defined in a separate DSL, similar to [Protocol Buffers](https://developers.google.com/protocol-buffers/). This enables code generation of much "boilerplate" code, and allows for high performing binary IO. More than this: event publishing is embedded into the domain objects, which has allowed us to roll out new features very quickly with high quality
+ Games and Lobbies are implemented as "Actors", which means each server node can scale to 1000s of concurrent players but the game logic is kept unaware of concurrency concerns
+ QA: The GS has a strong suite of automated tests on many levels. There are also several tools to aid development. 

### Poker Engine
The poker engine is completely decoupled from the "Hands Of Victory" extension. There are well defined extension points to allow for easy implementation of poker variants.

The decoupling is achieved by having the core poker model throw events for every state change. Extensions, such as Hands Of Victory and Hand History listen to and are driven by those core game events

Below is a part of one of the interfaces encapsulating those events. This interface is an example of code generated from the API/domain DSL. An implementation of the game model is also generated which handles the listeners and the dispatching of events. Since Java is a strongly typed language, it makes adding new features very easy - you get a lot of help from the compiler showing where all the updates need to be made
Equivalent boiler plate is also generated on the client side, which means that new features and new data automatically get propagated to the client

```java
public interface PublicPokerGameListener {
     void seatPlayerChanged(Integer seatIndex, PublicPlayer oldValue, PublicPlayer newValue);
     void playerUsernameChanged(Integer seatIndex, String oldValue, String newValue);
     void playerStackChanged(Integer seatIndex, Stack oldValue, Stack newValue);
     void playerVisibleCardAdded(Integer seatIndex, Card card);
     void playerVisibleCardsCleared(Integer seatIndex);
     void playerAccountBalanceChanged(Integer seatIndex, Long oldValue, Long newValue);
     void playerCurrentBetChanged(Integer seatIndex, Bet oldValue, Bet newValue);
     void playerLatestPokerActionChanged(Integer seatIndex, PokerAction oldValue, PokerAction newValue);
     void playerBettingStateChanged(Integer seatIndex, BettingState oldValue, BettingState newValue);
     void playerStateChanged(Integer seatIndex, PublicPlayerState oldValue, PublicPlayerState newValue);
     void communityCardAdded(Card card);
     void dealerButtonChanged(Integer oldValue, Integer newValue);
     void stateChanged(GameState oldValue, GameState newValue);
     void potAdded(Pot pot);
     void potsCleared();
     void potPotAmountAdded(Integer potIndex, PotAmount potAmount);
     void currentBetDecisionChanged(Integer oldValue, Integer newValue);
     void currentMuckDecisionChanged(Integer oldValue, Integer newValue);
     void bigBlindSeatChanged(Integer oldValue, Integer newValue);
     void bettingRoundChanged(Integer oldValue, Integer newValue);
     void resultChanged(Result oldValue, Result newValue);
     void currentMinimumBetChanged(Long oldValue, Long newValue);
     // ...
```
The hands-of-victory extension's model is also declared in the same way.
Here's a snippet:
```java
public interface HOVExtensionListener {
     void playerAdded(PlayerId playerKey, HOVPlayer hovPlayer);
     void playerPeekChanged(PlayerId playerKey, Peek oldValue, Peek newValue);
     void playerShownCardAdded(PlayerId playerKey, Card card);
     void playerPeekedCardAdded(PlayerId playerKey, Card card);
     void playerHandsWonChanged(PlayerId playerKey, Integer oldValue, Integer newValue);
     void playerFinalRankChanged(PlayerId playerKey, Integer oldValue, Integer newValue);
     void playerShowOneChanged(PlayerId playerKey, PlayerId oldValue, PlayerId newValue);
     void playerUncontestedHandsWonChanged(PlayerId playerKey, Integer oldValue, Integer newValue);
     void playerDeckStrengthChanged(PlayerId playerKey, Integer oldValue, Integer newValue);
     void playerResourceAdded(PlayerId playerKey, Resource resource);
     void playerAchievementAdded(PlayerId playerKey, Achievement achievement);
     void playerSharkAwardAdded(PlayerId playerKey, SharkAward sharkAward);
     void playerPointsChanged(PlayerId playerKey, Integer oldValue, Integer newValue);
     void latestCheckpointChanged(Checkpoint oldValue, Checkpoint newValue);
     void roundCountChanged(Integer oldValue, Integer newValue);
     void lastCallClockPlayerChanged(PlayerId oldValue, PlayerId newValue);
     void latestAbilityEventChanged(AbilityEvent oldValue, AbilityEvent newValue);
     void latestBonusEventChanged(BonusEvent oldValue, BonusEvent newValue);
     // ...
}
```
### Message Abstraction
The API/domain abstraction and the code generation framework allow us to implement system wide cross-cutting concerns in a general way. Every invocation on an api flows throw the system as a "message" which knows its API type and it's optional principal and target app object. For example, a player's betting action would have, say, `PokerGame` as its API type, the player's ID for "principal" and the game id as the "target app object". This message abstraction allows message routing to be generalised and decoupled from the individual APIs. Some messages may not have a principal or a specific target, for example a "CreateTournament" message, in which case there must only be one possible receiver for the message, say the "TournamentFactory" bean 

General behaviour, which is not API specific are implementations of the `MessageHandler` abstraction.

Messages must also know how to invoke themselves on the "real" target:

In java:
```java
public interface Message<I,V> {
    Class<I> api();        //the group this message belongs to
    String entityKey();    //the target if any
    Principal principal(); //who sent this message?
    V visit(I in);         //invoke appropriate method on the target bean
}
```

### Actors 
Each Actor (for example a poker sitngo) has an inbox of these messages which the Actor framework invokes on the target bean in sequence. Actors and beans are loosely coupled so that an arbitrary collection of beans can share the same "mailbox". This ensures that all access to the cluster of objects is synchronized without requiring a separate thread for each. For example, a game on HandsOfVictory.com will have one "key" (which acts as the message routing address) but contains a core poker game object + a hands of victory extension object and a chat object

This snippet shows some of this plumbing
```java
        public class ActorMailbox<A> implements ActorMailbox<A> {
            private final ActorManager actorManager;
            private final A delegate;
        
            // ...
        
            public <T> T handleMessage(Message<A, T> message) {
                String entityKey = message.entityKey();
                actorManager.post(entityKey, new Task<T>(message));
                return null;
            }
        
            private class Task<T> implements Runnable {
                private final Message<A, T> message;
        
                public void run() {
                    message.visit(delegate);
                    //handle error
                }
            }
        }

```

### Distribution Model
The GS can be distributed over several JVMs (nodes). Each node will run the same codebase but can be configured to take on different roles, for example "poker game server", "lobby", "tournament server", "connection server". In a single node deployment, the single node will take on all roles.

Nodes communicate with each other using Hazelcast. Hazelcast is primarily a shared memory solution, but the only thing stored in shared memory is  a service and object address registry. Messages are sent between nodes using the message abstraction described above. The code generation framework makes it practical to use a very fast binary protocol for serializing messages between nodes.

Domain objects such as poker games, tournaments and lobbies will reside on a single node (lobbies can be replicated). Message routing is handled on a low level because all messages "know" which API they are bound for, and optionally which object. This allows us to decouple, for example, a server node which handles connections to clients, from the application objects (games, tournaments, lobbies etc) themselves.

### Persistence
The Game Server does not need a relational DB for persistence. Entities like "Users" should be handled by the platform, as should the persistence of monetary transactions, accounts and hand-history. Out of the box, the GS does support a rudimentary style of file based persistence for user accounts but in a production environment these kind of services should be swapped out

#### GS Persistence Mechanism
The GS uses a simple file based storage mechanism built on to the MessageHandler/Observer framework described above. Each persisted object is stored as a file in a key/value based storage. The Persistence Mechanism is "just another" listener to deltas on the object and appends the deltas to the file. Each file has an object snapshot as the first line followed by the deltas on each line. This means persistence is optimised for writing but slow for reading - which is fine because GS uses persistence for back up only.

_Money Sessions_

Although possible, the GS does not currently back up all changes to running poker games. In the rare event of a GS crash the running games are lost. However, in the case of cashgames for example, the GS _does_ guarantee the money brought to the table. A subset of the game state (the part concerning the money in play and to whom it belongs to) _is_ persisted in the form of a money session. When the GS node is restarted, the saved money sessions are scanned and refund requests are sent to the platform.

A similar strategy is used for Sngs and Tournaments whereby although the GS does not currently implement restoring of a crashed tournament, it _does_ guarantee the entrance fees will be tracked and refunded if necessary

_Other things Persisted by GS_

Each GS node will persist its offering in terms of recurring and scheduled tournaments, cash games and SNG waiting lists. If a GS node is brought down and back up again, all tournaments will be reinstated with all current registrations. The same applies to Sng Waiting Lists. As mentioned, the GS won't try and re-create running tournaments or cash games, but will guarantee refunds for those.

### Back End Integration
The GS is designed to be pluggable into an existing back-end. A small integration component must be written depending on the backend. A "default" implementation exists as a reference which handles users, money accounts and other things like hand-history internally.

Generally a "back-end" (aka platform) could potentially be interested in any GS event and the GS has been designed with that in mind - to make new integration points easy to add. Hands Of Victory is an example of a "Special" integration. See below

#### Wallet - Real Money
The platform should provide access to money accounts so that users can bring money in to the games. As mentioned above, when money is "brought in" to the GS for example by the purchase of a tournament ticket or buying in to a cash game, the GS must guarantee to keep track of that money even in the event of a server crash
The GS assumes that accounts can be differentiated in several dimensions. Those "dimensions" could be for example "real" vs "play" money, or different operators if running in a network, or different currencies. Essentially the GS itself doesn't care what those dimensions are - just that it must faithfully track the amounts
#### User Management
The platform should provide the GS with users and provide authentication. The clients connect to the GS using web-sockets and must send an auth token provided by the platform. The GS should then be able to authenticate the user with the token using the backend endpoint. 
#### Hand History
Hand History is "just another" listener on poker game events and as such is decoupled from the Poker engine. The platform can receive all the information from all poker hands and filter it how it likes. For example, maybe it won't bother persisting anything for play money games. Or perhaps it just want to persist the result of every hand - e.g. who won what with what cards.

If it likes, the backend can receive everything, this opens up interesting possibilities. Storage is cheap, so why not store it all? The GS will send the initial game state for each hand along with all events from the hand, including the timestamps of when they occurred. This means that a slightly modified client app can be used as a hand player with real time playback. Hand history can become valuable content in its own right. Users can share their favourite hands, commentary can be added, play can be "streamed" in near real time to a large audience without affecting the core game engine. 
#### "Special" Integrations
Hands of Victory (HOV) requires a special integration which is a small component separate from the above integrations. The HOV backend is interested in a different subset of events than those described above. The HOV integration is done over REST, but as with all APIs it is defined the HighLevel DSL, and the GS interacts with the following interface (the fact that it's REST is implemented by a generic stub which can be easily swapped out for something else)
```java
      public interface HOVIntegration {
      
           StartGameSessionResponse startGameSession(List<String> playerSessionIds, String ngTourKey, String gameType, Integer numSeats) throws GenericException;
           void playerEliminated(String playerSessionId, SngResult result) throws GenericException;
           void gameComplete(String gameSessionId) throws GenericException;
           GetPlayerResponse getPlayer(String userId) throws GenericException;
           AuthenticatePlayerResponse authenticatePlayer(String authToken) throws GenericException;
           CreatePlayerSessionResponse createPlayerSession(String authToken) throws GenericException;
           void removePlayerSession(String playerSessionId) throws GenericException; //message sent when a player leaves, say a waiting list, without ever playing the match
      }
```
In addition, here is part of the "special" integration API for the HOV clients
```java
    public interface HOVServer {
        LoginWithPlayerSessionResponse hovLogin(String authToken, UseCase useCase) throws GenericException;
        StartSinglePlayerGameResponse startSinglePlayerGame(String authToken, String userId, Integer botCount, PokerCharacter pokerCharacter, Integer botDecisionTime) throws GenericException;
         LoginAsObserverResponse loginAsObserver(String userId) throws GenericException;
    }
```
The interesting thing here is it only takes a small amount of special integration components to allow a completely fresh way of interacting with what is essentially a "classic", though advanced, poker platform

### Supported Application Objects
The GS can be split into two categories of component. One category is the low level server related functions, such as persistence, message routing, and concurrency handling. The other category are "application objects" which are essentially the applications that run inside the GS
#### Channel
Most application objects derive from the "Channel" concept. A channel is something that users can "join" and "leave" and subscribe to events from
#### Chat
Chat can easily be added to any channel and can be thought of as the simplest application object you could have
#### Poker Games
Poker game "channels" support up to N players playing on seats at a table. The Poker Channel encapsulates a running poker game. It can run with "extensions" - hands of victory being a very sophisticated example. They can also be wrapped in different "contexts" such as 

+ "cash game" (which additionally handles seating players, reserving money etc)
+ "tournament" where seating is controlled by an external application objects in communication with this one. This context is also used for SNGs, which from the poker game's point of view are the same

#### Tournaments
tournaments are application objects which manage the lifecycle of a poker tournament (although it needn't necessarily be a poker tournament). Scheduled tournaments can live for a long time before they are running while players register to it. They have a fixed start time. Whilst running the tournament app object will track the overall management of the tournament. Clients can subscribe to the tournament and show a separate view for it, for example to give an overview of how everyone is doing, where they are seated, etc.
#### Recurring Tournaments
Although not really a channel (because clients can't join and subscribe to it), a recurring tour is an app object which ensures that scheduled tours are pre-created in advance. This allows an administrator to set up a tournament and apply a "recur rule" to ensure that a certain kind of tournament will run at regular intervals
#### Sng Waiting Lists
Sng Waiting Lists are channels that users can join to play tournaments on-demand. New tournaments are launched every time a certain number of required players are registered
#### Lounge
A lounge is similar to a SNG Waiting list, except tournaments are kicked off by the players themselves (or rather a player who has the rights to do so) instead of automatically. A lounge would typically have a chat channel attached so players can communicate. In HOV we used a communal lounge to help welcome new players to the game and to allow our own people to interact with the players. Lounges could also be used to allow players to hold private games and tournaments
#### Melee Tournaments
Melee tournaments are a unique tournament concept which, for a given set of players, will start a new tournament every X minutes with the same set of players. In this format, a player's ability to cope with playing multiple games at once is tested
#### Multi Tournaments
Multi tournaments are a bit like Melees, but X tournaments are launched with the same players at the same time
#### Poker Extensions - Hands Of Victory
As mentioned, HOV is a "poker extension". It runs as an app object inside the same "cluster" of objects as the poker game itself. It listens to Core Poker Game events and also has its own API with the clients. Being in the same "cluster" means it has the same "address" for messages, and all messages are run on the cluster in sequence. I.e. the poker game, the hov extension and the chat component are combined into one "actor"
#### Lobbies
Lobbies are app objects which collect information (static and dynamic) about other app objects. For example there would typically be a "cashgame lobby", a "tournament lobby" or a "sng lobby". Clients can join lobbies and subscribe to changes for a subset of their contents. App objects can be attached to a "LobbyListener" (just another event listener) which will relay updates to a lobby. Lobbies will typically track a small subset of the app object's events such as who is sitting down, how big is their stack etc
#### News Feed
A News Feed app object is a simpler more general concept than a lobby and can be used as a way of broadcasting interesting events to interested clients. This could be used, say, on the portal's front page to show a live feed of things like "X logged in", "Y joined Sng Z" etc

### Client/Server Comms
HOV clients open up secure websocket connections with the server and requests/responses/notifications are sent as Json. A fairly cheap performance enhancement would be to switch this to a binary protocol and perhaps use HTTP/2 instead

A layer of javascript is generated from the higher-level domain definition. This layer provides hooks to attach to all game events, and maintains a client side copy of the public and private (the player's own cards for example) game state. 

### Javascript Client

The javascript client consists of implementations of all the function skeletons generated from the protocol. The generated code provides useful structure to help manage the complexity of the poker client. JQuery and a simple js animation framework is used to give a pretty good UI experience using CSS3 animations

### Future Work/Opportunities
The GS is an excellent platform for innovating new poker products. Hands Of Victory goes a long way to prove this. It has all the ingredients necessary to form the base of a new wave of poker products, and with minimal effort could support a very large full featured Real Money or FTP poker site. The unique all-in approach to managing all APIs in our DSL and generating code from that makes a lot of very difficult problems easier for us to achieve:

+ failover for very large tournaments
+ scaling up to many servers
+ extending poker in new and exciting ways
+ allow hand history to be used as sharable content



