//start Principal
// Encapsulates data about the player performing requests, e.g. player id, session id etc
var Principal = function() {
    this.playerId        = null; // PlayerId unique id of player in game server
    this.playerSessionId = null; // String   id of the player's sessions within the game server. Will encompass for example, one handsofvictory tournament or trial game
};
//end Principal
//start PlayerId
// unique identifier for player
var PlayerId = function() {
    this.value = null; // String
};
//end PlayerId
//start UserInfo
// user details carried over from web env/platform
var UserInfo = function() {
    this.firstName = null; // String
    this.lastName  = null; // String
    this.password  = null; // String
    this.nickname  = null; // String
    this.country   = null; // Country  ()
    this.email     = null; // String
};
//end UserInfo
//start ImageData
// contains the binary data of a jpg image
var ImageData = function() {
    this.data = null; // Byte[]
};
//end ImageData
//start PublicTour
// Models all the common tournament management state which is visible to all players and observers
var PublicTour = function() {
    this.key              = null; // String                the key of this tournament channel in the game server
    this.registeringState = null; // RegisteringState      Holds the registered players to a tournament. Will be nulled out when the tournament starts and moves to RUNNING
    this.runningState     = null; // RunningState          Holds the state of the tournament (players, stacks, locations etc) while a tournament is running
    this.tourSettings     = null; // ScheduledTourSettings static settings which configured this tournament
    this.state            = null; // TourState             enum label describing the tournament state  (ANNOUNCED,REGISTERING,RUNNING,FINISHED,CANCELLED)
    this.parentKey        = null; // String                key of the parent tournament - only applicable for certain tournament variants, n/a in handsofvictory
    this.externalTourKey  = null; // String                reference for this tournament in the backend system (i.e. the php web app for handsofvictory)
};
PublicTour.prototype.registeringStateChanged = function(oldValue,newValue) {
    this.registeringState = newValue;
};
PublicTour.prototype.registeringStateEntrantAdded = function(entrant) {
    this.registeringState.entrants.push(entrant);
};
PublicTour.prototype.registeringStateEntrantsAdded = function(entrants) {
    this.registeringState.entrants = this.registeringState.entrants.concat(entrants);
};
PublicTour.prototype.registeringStateEntrantRemoved = function(entrantIndex) {
    this.registeringState.entrants.splice(entrantIndex, 1)
};
PublicTour.prototype.registeringStateEntrantsCleared = function() {
    this.registeringState.entrants = [];
};
PublicTour.prototype.entrantRegisterStateChanged = function(entrantIndex,oldValue,newValue) {
    this.registeringState.entrants[entrantIndex].registerState = newValue;
};
PublicTour.prototype.runningStateChanged = function(oldValue,newValue) {
    this.runningState = newValue;
};
PublicTour.prototype.playerStackChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].stack = newValue;
};
PublicTour.prototype.playerFinalRankChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].finalRank = newValue;
};
PublicTour.prototype.playerPrizeChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].prize = newValue;
};
PublicTour.prototype.playerEliminatedChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].eliminated = newValue;
};
PublicTour.prototype.playerMoveCountChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].moveCount = newValue;
};
PublicTour.prototype.playerMarkedForMoveChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].markedForMove = newValue;
};
PublicTour.prototype.playerRankChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].rank = newValue;
};
PublicTour.prototype.playerQuitChanged = function(playerIndex,oldValue,newValue) {
    this.runningState.players[playerIndex].quit = newValue;
};
PublicTour.prototype.runningStateStateChanged = function(oldValue,newValue) {
    this.runningState.state = newValue;
};
PublicTour.prototype.tableSeatAdded = function(tableIndex,tSeat) {
    this.runningState.tables[tableIndex].seats.push(tSeat);
};
PublicTour.prototype.tableSeatsAdded = function(tableIndex,tSeats) {
    this.runningState.tables[tableIndex].seats = this.runningState.tables[tableIndex].seats.concat(tSeats);
};
PublicTour.prototype.tableSeatRemoved = function(tableIndex,seatIndex) {
    this.runningState.tables[tableIndex].seats.splice(seatIndex, 1)
};
PublicTour.prototype.tableSeatsCleared = function(tableIndex) {
    this.runningState.tables[tableIndex].seats = [];
};
PublicTour.prototype.seatTourPlayerIdChanged = function(tableIndex,seatIndex,oldValue,newValue) {
    this.runningState.tables[tableIndex].seats[seatIndex].tourPlayerId = newValue;
};
PublicTour.prototype.tableIdleChanged = function(tableIndex,oldValue,newValue) {
    this.runningState.tables[tableIndex].idle = newValue;
};
PublicTour.prototype.tableRemovedChanged = function(tableIndex,oldValue,newValue) {
    this.runningState.tables[tableIndex].removed = newValue;
};
PublicTour.prototype.runningStateCurrentLevelChanged = function(oldValue,newValue) {
    this.runningState.currentLevel = newValue;
};
PublicTour.prototype.runningStatePauseTimestampChanged = function(oldValue,newValue) {
    this.runningState.pauseTimestamp = newValue;
};
PublicTour.prototype.stateChanged = function(oldValue,newValue) {
    this.state = newValue;
};
PublicTour.prototype.externalTourKeyChanged = function(oldValue,newValue) {
    this.externalTourKey = newValue;
};
//end PublicTour
//start RegisteringState
// tournaments registering state before the tour starts. Not used in handsofvictory, except for a few milliseconds while the SNG is automatically set up by the game server. In a scheduled tournament, this would track registered players over a longer period of time
var RegisteringState = function() {
    this.entrants = null; // Entrant[] Models the entrants registered to the tournament
};
//end RegisteringState
//start Entrant
var Entrant = function() {
    this.principal     = null; // Principal     handle to the player/user in the game server
    this.nickname      = null; // String        their nickname shown to other players
    this.registerState = null; // RegisterState n/a on the client. But tracks state on server to handle async registration with backend system (APPROVED,UNAPPROVED,UNREGISTER_PENDING)
    this.attachment    = null; // Object        extension configuration/settings for the player. Will be set by backend and read, in our case, by the handsofvictory extension
};
//end Entrant
//start RunningState
// Tournament state for a running tournament
var RunningState = function() {
    this.players        = null; // TourPlayer[]     Encapsulates all the participants in the tournament, and their state
    this.state          = null; // RunningTourState enum label describing the state of the running tournament (PAUSED,RUNNING,HAND_BY_HAND,BREAK,PREPARE)
    this.tables         = null; // TTable[]         models all the tables in a tournament. There is only ever one table in a handsofvictory tournament
    this.currentLevel   = null; // LevelInfo        describes the current level in the tournament. This is not applicable in handsofvictory because the HOV extension manages the levels itself
    this.pauseTimestamp = null; // Long             n/a in handsofvictory
};
//end RunningState
//start TourPlayer
// encapsulates state for a player in a tournament. Typically tournament bookkeeping - no poker specific state
var TourPlayer = function() {
    this.tourPlayerId  = null; // Integer   unique id for the player within this tournament
    this.principal     = null; // Principal handle to the user/player
    this.nickname      = null; // String
    this.stack         = null; // Long      tracks the player's stack (score) in the game.
    this.finalRank     = null; // Integer   gets a value when a player is eliminated. 1=winner
    this.prize         = null; // Prize     n/a in handsofvictory
    this.eliminated    = null; // Boolean   true after elimination
    this.moveCount     = null; // Integer   n/a in hov
    this.markedForMove = null; // Boolean   n/a in hov. Players never move tables
    this.rank          = null; // Integer   current rank, changes while player is still in tournament.. Not needed in hov client
    this.quit          = null; // Boolean   true if the player elects to quit the game. We track quit players so that we can end  the tournament early if all but one quit
    this.attachment    = null; // Object    extension specific data. Does not need to be read by the hov client
};
//end TourPlayer
//start Prize
// n/a in hov
var Prize = function() {
};
//end Prize
//start TTable
// player indexes refer to the index of players in the player list. Not needed by hov client, because we only have single table tournaments
var TTable = function() {
    this.tableIndex = null; // Integer effectively the id of the table within the tour..
    this.ref        = null; // String
    this.seats      = null; // TSeat[]
    this.idle       = null; // Boolean
    this.removed    = null; // Boolean
};
//end TTable
//start TSeat
var TSeat = function() {
    this.index        = null; // Integer
    this.tourPlayerId = null; // Integer
};
//end TSeat
//start LevelInfo
// wraps a level plus its start time
var LevelInfo = function() {
    this.level     = null; // Level
    this.startTime = null; // Long
    this.levelLeft = null; // Long
};
//end LevelInfo
//start Level
// n/a in hov. Levels are managed by the hov extension itself
var Level = function() {
    this.levelIndex = null; // Integer
    this.smallBet   = null; // Long
    this.ante       = null; // Long
};
//end Level
//start ScheduledTourSettings
// toursettings plus an open time and start time. Not really relevant for hov
var ScheduledTourSettings = function() {
    this.settings  = null; // TourSettings
    this.openTime  = null; // Long
    this.startTime = null; // Long
};
//end ScheduledTourSettings
//start TourSettings
var TourSettings = function() {
    this.gameType         = null; // GameType          LIMITED_DECK_PINEAPPLE in hov (TEXAS_HOLDEM,SEVEN_CARD_STUD,OMAHA_HI,OMAHA_HI_LO,FIVE_CARD_DRAW,CRAZY_PINEAPPLE,CHEAT,LIMITED_DECK_PINEAPPLE)
    this.bettingType      = null; // BettingType       NO_LIMIT for hov (NO_LIMIT,FIXED_LIMIT,POT_LIMIT)
    this.seatsPerTable    = null; // Integer           typically 2, 4 or 6 in hov..
    this.initialStackSize = null; // Long              typically 50 in hov
    this.buyIn            = null; // Long              n/a in hov
    this.fee              = null; // Long              n/a in hov
    this.accountType      = null; // AccountType       n/a in hov
    this.name             = null; // String            n/a in hov
    this.password         = null; // String            n/a in hov
    this.minPlayers       = null; // Integer           typically 2, 4 or 6 in hov..
    this.maxPlayers       = null; // Integer           typically 2, 4 or 6 in hov..
    this.type             = null; // String            n/a for hov
    this.levelLength      = null; // Float             n/a in hov
    this.levelStrategy    = null; // LevelStrategyEnum CONTROLLED_BY_GAME for hov (REGULAR,CONTROLLED_BY_GAME)
};
//end TourSettings
//start AccountType
// n/a in hov
var AccountType = function() {
    this.moneyType = null; // MoneyType  (REAL_MONEY,PLAY_MONEY)
    this.currency  = null; // String
};
//end AccountType
//start PrivateTourState
// state only sent to the player concerned. State is either private or superfluous to present a 3rd person view
var PrivateTourState = function() {
    this.playerId            = null; // PlayerId
    this.privateRunningState = null; // PrivateRunningState state which is initialized when the tournament actually starts
    this.registered          = null; // Boolean             n/a in hov. Players don't unregister from a tournament (although they can leave a sng waiting list)
};
PrivateTourState.prototype.privateRunningStateChanged = function(oldValue,newValue) {
    this.privateRunningState = newValue;
};
PrivateTourState.prototype.privateRunningStateMyTableKeyChanged = function(oldValue,newValue) {
    this.privateRunningState.myTableKey = newValue;
};
PrivateTourState.prototype.privateRunningStateMyRankChanged = function(oldValue,newValue) {
    this.privateRunningState.myRank = newValue;
};
PrivateTourState.prototype.privateRunningStateMyStackChanged = function(oldValue,newValue) {
    this.privateRunningState.myStack = newValue;
};
PrivateTourState.prototype.privateRunningStateMyTourPlayerIdChanged = function(oldValue,newValue) {
    this.privateRunningState.myTourPlayerId = newValue;
};
PrivateTourState.prototype.registeredChanged = function(oldValue,newValue) {
    this.registered = newValue;
};
//end PrivateTourState
//start PrivateRunningState
// private state which is initialized when the tournament starts
var PrivateRunningState = function() {
    this.myTableKey     = null; // String  game server id of the table which I should join. This IS used in hov to allow the client to figure out which table channel to join. This is one of the few uses of joining the tournament channel at all.
    this.myRank         = null; // Integer tracks my overall rank in the tournament.. N/A in hov, and any single table tournament, because it is the same as rank at table
    this.myStack        = null; // Long    n/a in a sng table tournament - since we don't need a tour overview UI in this case.
    this.myTourPlayerId = null; // Integer my tournament scoped unique id
};
//end PrivateRunningState
//start PublicPlayer
// All the player state which is visible to other players and observers
var PublicPlayer = function() {
    this.id                = null; // PlayerId          identifies the player within the game server
    this.username          = null; // String            player's username as set in handsofvictory web app
    this.stack             = null; // Stack             the player's stack
    this.visibleCards      = null; // Card[]            the player's visible cards. For example will have values during showdown ()
    this.accountBalance    = null; // Long              n/a in hov
    this.currentBet        = null; // Bet               has a value during a betting round. Represents this player's current bet
    this.noHiddenCards     = null; // Integer           number of hidden cards. Will be 3 in hov while pineapple selection takes place
    this.seatIndex         = null; // Integer           this player's seat index
    this.latestPokerAction = null; // PokerAction       the last poker action this player performed. Nulled at end of betting round (BET,RAISE,CALL,CHECK,FOLD,INSURE)
    this.bettingState      = null; // BettingState      describes poker betting state. A UI may ignore this, or could use colours to help visualise (ALL_IN,FOLDED,PASSIVE,ACTION_REQUIRED,CHECKABLE,ALL_IN_PROTECT)
    this.highCard          = null; // Card              has a value at start of game when high carding for dealer button takes place ()
    this.state             = null; // PublicPlayerState most states are only applicable in cash games. always PLAYING in hov (PLAYING,WANT_TO_PLAY_ASAP,WAITING_FOR_BIG_BLIND,RESERVING,IDLE,WANT_TO_SIT_OUT)
    this.disconnected      = null; // Boolean           true if the (other) player is disconnected from server - obviously if I am disconnected, this flag won't get set
    this.markedForRemoval  = null; // Boolean           n/a in hov
    this.awayState         = null; // AwayState         away players are auto check/folded. Quit players cannot come back. (PRESENT,AWAY,QUIT)
    this.winChances        = null; // WinChances        maps betting round to a percent win chance for this player. Used to display the all in sequence.. All figures are precalculated on the server
};
//end PublicPlayer
//start Stack
// models a player's stack of chips at the table
var Stack = function() {
    this.amount       = null; // Long            the amount of chips
    this.latestUpdate = null; // StackUpdateType describes the last change to the stack. May be useful for client to differentiate how the stack change is animated (REBUY,BUY_IN,BET,BLIND,ANTE,RETURNED_BET,POT_WIN,NONE,TOP_UP,CHANGED_BY_EXTENSION)
};
//end Stack
//start Bet
// describes the amount and type of bet
var Bet = function() {
    this.amount  = null; // Long
    this.betType = null; // BetType  (BET,BIG_BLIND,SMALL_BLIND,ANTE,NONE)
};
//end Bet
//start WinChances
// array of win chances, if all in preflop then the array will contain a value for preflop,flop,turn and river chances
var WinChances = function() {
    this.chances = null; // Integer[]
};
//end WinChances
//start Pot
// describes one of the pots in a game
var Pot = function() {
    this.potAmounts    = null; // PotAmount[] describes how the pot is made up. Which bets come from which players
    this.deadBetAmount = null; // Long        n/a for hov
};
//end Pot
//start PotAmount
// tuple of player to amount bet
var PotAmount = function() {
    this.seat   = null; // Integer
    this.amount = null; // Long
};
//end PotAmount
//start Result
// describes in detail the result of one hand
var Result = function() {
    this.potAllocation = null; // PotAllocation[] one entry for each pot
    this.shownHands    = null; // ShownHand[]     describes all the non-mucked hands
};
//end Result
//start PotAllocation
// used in results to describe who won what amount from each pot. (sometimes pots are split)
var PotAllocation = function() {
    this.potAmounts = null; // PotAmount[]
};
//end PotAllocation
//start ShownHand
// describes a hand shown in showdown. Used to visualise the evaluated hand
var ShownHand = function() {
    this.seat        = null; // Integer
    this.hand        = null; // HandToken
    this.pocketCards = null; // Card[]     ()
};
//end ShownHand
//start HandToken
// This is a special type and has fields not shown here. See function handString in hov-client.js. Each hand token has a subtype, e.g. 'Straight' which has a cardHigh and low field
var HandToken = function() {
    this.cards = null; // Card[]  ()
};
//end HandToken
//start ChatMessage
// encapsulates a table chat message
var ChatMessage = function() {
    this.playerId = null; // PlayerId
    this.sender   = null; // String
    this.message  = null; // String
};
//end ChatMessage
//start FixedBetSettings
// describes the current (static in hov) fixed bets applied for each hand
var FixedBetSettings = function() {
    this.smallBlind            = null; // Long
    this.bigBlind              = null; // Long
    this.ante                  = null; // Long ante that's debited from the players stack
    this.communalAnte          = null; // Long ante thats added to the game for each hand. Players don't pay it from their own stacks
    this.communalAntePerPlayer = null; // Long
};
//end FixedBetSettings
//start LimitedDeckPlayer
var LimitedDeckPlayer = function() {
    this.playerId            = null; // PlayerId
    this.config              = null; // LimitedDeckPlayerConfig the fixed config for this player
    this.peek                = null; // Peek                    has a value if another player is peeking at this player
    this.shownCards          = null; // Card[]                  n/a for hov, but players can show cards at end of hand to show they weren't bluffing etc ()
    this.peekedCards         = null; // Card[]                  cards that are visible to all due to a public peek (Expose) ()
    this.handsWon            = null; // Integer                 counter tracking hands won
    this.username            = null; // String
    this.finalRank           = null; // Integer
    this.seatIndex           = null; // Integer
    this.showOne             = null; // PlayerId                id of player who has looked at a card of this player (eye in the sky)
    this.uncontestedHandsWon = null; // Integer                 tracks how many hands were won uncontested.. Could be used in bonus/awards etc
    this.blocked             = null; // Boolean                 n/a in hov - but one ability that is not used now is the ability to block another from using their abilities
    this.deckStrength        = null; // Integer                 a score describing the current strength of the players limited deck
    this.resources           = null; // Resource[]              resources are listed here, because they can only be spent if in this array.. Helps client validate whether certain actions can be performed (EXTRA_LIFE,MIND_TRICK,INSURE,MECHANICS_GRIP,TOP_UP)
    this.achievements        = null; // Achievement[]           achievements won during this hov match (CHIP_AND_CHAIR,CRUISE_TO_VICTORY,POCKETS_GALORE,RED_HANDED,OUTPLAYED,WILD_THING,CLEAN_SWEEP,PERFECT_READS)
    this.sharkAwards         = null; // SharkAward[]            shark awards won during this hov match
    this.points              = null; // Integer                 current score
    this.cunning             = null; // Integer                 current cunning (used to perform actions, but not counting as score at end of game)
};
//end LimitedDeckPlayer
//start LimitedDeckPlayerConfig
// fixed starting config for hov players
var LimitedDeckPlayerConfig = function() {
    this.startSharkPoints     = null; // Integer        points a player starts the game with
    this.pokerCharacter       = null; // PokerCharacter token representing the hov character (NONE,CAT,BIG_STACK,GENERAL,SHUFFLER,GUNG_HO,FINGERS,WUNDERKIND,HOLLYWOOD,TIGER_LILY)
    this.abilities            = null; // Move[]         the abilities this player has - typically actions they can perform
    this.edges                = null; // Move[]         the edges they have - typically these happen automatically
    this.edgeBonuses          = null; // EdgeBonus[]    bonus awards this player can win if they play in a certain way (SIXTH_SENSE,IN_FOR_THE_MAX,PHOENIX,HOUSE_RULES,CHICKENS,READ_AND_WEEP,FEAR_THE_FIST,SHOWBOATING)
    this.possibleAchievements = null; // Achievement[]  achievements this player are eligable for (CHIP_AND_CHAIR,CRUISE_TO_VICTORY,POCKETS_GALORE,RED_HANDED,OUTPLAYED,WILD_THING,CLEAN_SWEEP,PERFECT_READS)
};
//end LimitedDeckPlayerConfig
//start Move
// describes an ability coupled with its cost
var Move = function() {
    this.specialPlay = null; // Ability  (MECHANICS_GRIP,EXPOSE,EYE_IN_THE_SKY,CALL_CLOCK,ADD_TIME,SWAP,BLOCK,TOP_UP,INSURE,REBUY,MIND_TRICK,SOUL_READ,STRADDLE)
    this.cost        = null; // Integer cost to perform in cunning or points
};
//end Move
//start Peek
// encapsulates a peek at a players hole cards
var Peek = function() {
    this.peeker     = null; // PlayerId
    this.publicPeek = null; // Boolean
};
//end Peek
//start SharkAward
// describes a point award or penalty. Negative points means penalty
var SharkAward = function() {
    this.type    = null; // SharkAwardType  (STACKED,FIRST_BLOOD,STAYING_ALIVE,DOUBLE_TROUBLE,CRUSHING,BUSTO,PERFECT_PERFECT,WIN,EPIC_WIN,ACES_DOWN,RUNNER_UP,EPIC_LOSS,OFF_TO_THE_RACES,NOT_A_FLUKE,COIN_FLIP,DOMINATED,GOTCHA,SMOOTH_SAILING,IN_CONTROL,DOUBLE_UP,HERO_FOLD,DODGED_BULLET,DOWN_TO_TWO,HERO_CALL,RUN_FOR_THE_HILLS,IN_THE_DRIVERS_SEAT,WIN_STREAK_SMALL,WIN_STREAK_LARGE,LOSE_STREAK_SMALL,LOSE_STREAK_LARGE,CARD_BONUS)
    this.points  = null; // Integer        the value to add to points
    this.cunning = null; // Integer        the value to add to cunning
};
//end SharkAward
//start AbilityEvent
// encapsulates the use of an action by one player (possibly on another)
var AbilityEvent = function() {
    this.playerId    = null; // PlayerId
    this.ability     = null; // Ability   (MECHANICS_GRIP,EXPOSE,EYE_IN_THE_SKY,CALL_CLOCK,ADD_TIME,SWAP,BLOCK,TOP_UP,INSURE,REBUY,MIND_TRICK,SOUL_READ,STRADDLE)
    this.counterpart = null; // PlayerId optional counterpart, e.g. eye in the sky has a counterpart
    this.edge        = null; // Boolean  is this classed as an edge (may affect how action is visualised)
};
//end AbilityEvent
//start BonusEvent
// encapsulates the moment when a bonus is awarded
var BonusEvent = function() {
    this.playerId = null; // PlayerId
    this.bonus    = null; // EdgeBonus  (SIXTH_SENSE,IN_FOR_THE_MAX,PHOENIX,HOUSE_RULES,CHICKENS,READ_AND_WEEP,FEAR_THE_FIST,SHOWBOATING)
};
//end BonusEvent
//start BonusCard
// n/a in hov.. not yet used
var BonusCard = function() {
    this.card   = null; // Card     ()
    this.points = null; // Boolean
    this.amount = null; // Integer
};
//end BonusCard
//start PublicPokerGame
// The poker game state which is common for all players and observers in a poker game. E.g. the community cards
var PublicPokerGame = function() {
    this.key                 = null; // String           identifies the game inside the server. Used to route messages to this actor in the server.
    this.settings            = null; // GameSettings     All the configurable static properties for the poker game
    this.seats               = null; // Seat[]           array of the seats. Seats can be empty.
    this.communityCards      = null; // Card[]           tracks the community cards. Cleared at end of SHOWDOWN, added on state change to DEALING_FLOP,DEALING_TURN,DEALING_RIVER  ()
    this.dealerButton        = null; // Integer          seatindex of player who is dealer
    this.state               = null; // GameState        marker label changing for each state of the game. e.g. DEALING_CARDS,COLLECTING_POT etc. All states last for a configurable amount of time and correspond the amount of time the client gets to animate the state (WAITING,EXPECTING_MUCK_DECISION,EXPECTING_BET_DECISION,EXPECTING_CARD_SELECTION,DEALING_CARDS,EXPECTING_BLINDS_DECISION,PAYING_BLINDS,HIGH_CARD_FOR_BUTTON,DEALING_FLOP,DEALING_TURN,DEALING_RIVER,ENDING_HAND,ALL_IN_SEQUENCE,SHOWDOWN,POST_BET_DECISION,POST_BETTING_ROUND,SHUFFLING_CARDS,PAYING_ANTES,POST_ROUND,MORE_TIME_REQUESTED,CALL_CLOCK_REQUESTED,PAUSED)
    this.pots                = null; // Pot[]            has multiple values when players go all in. First entry is main pot, subsequent entries are side pots
    this.currentBetDecision  = null; // Integer          the seat index of the player whose turn it is
    this.currentMuckDecision = null; // Integer          not used for hands of victory
    this.bigBlindSeat        = null; // Integer          seatindex of player to post the big blind.
    this.bettingRound        = null; // Integer          index of betting round 0=preflop, 1=flop etc
    this.result              = null; // Result           complex object describing result. Value set at SHOWDOWN start. Value nulled out after SHOWDOWN before POST_ROUND
    this.gameContext         = null; // GameContext      enum flag, CASH_GAME or TOURNAMENT. Always TOURNAMENT in handsofvictory (CASHGAME,TOURNAMENT)
    this.chatMessages        = null; // ChatMessage[]    stores latest n table chat messages
    this.currentMinimumBet   = null; // Long             the current minimum bet allowed. Allows client to block too small bets
    this.roundCounter        = null; // Long             counter which counts the rounds player since tournament start
    this.markedForRemoval    = null; // Boolean          server flag. Not used in handsofvictory
    this.fixedBetSettings    = null; // FixedBetSettings details current blind levels, antes etc
};
PublicPokerGame.prototype.seatPlayerChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player = newValue;
};
PublicPokerGame.prototype.playerUsernameChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.username = newValue;
};
PublicPokerGame.prototype.playerStackChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.stack = newValue;
};
PublicPokerGame.prototype.playerVisibleCardAdded = function(seatIndex,card) {
    this.seats[seatIndex].player.visibleCards.push(card);
};
PublicPokerGame.prototype.playerVisibleCardsAdded = function(seatIndex,cards) {
    this.seats[seatIndex].player.visibleCards = this.seats[seatIndex].player.visibleCards.concat(cards);
};
PublicPokerGame.prototype.playerVisibleCardRemoved = function(seatIndex,visibleCardIndex) {
    this.seats[seatIndex].player.visibleCards.splice(visibleCardIndex, 1)
};
PublicPokerGame.prototype.playerVisibleCardsCleared = function(seatIndex) {
    this.seats[seatIndex].player.visibleCards = [];
};
PublicPokerGame.prototype.playerAccountBalanceChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.accountBalance = newValue;
};
PublicPokerGame.prototype.playerCurrentBetChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.currentBet = newValue;
};
PublicPokerGame.prototype.playerNoHiddenCardsChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.noHiddenCards = newValue;
};
PublicPokerGame.prototype.playerLatestPokerActionChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.latestPokerAction = newValue;
};
PublicPokerGame.prototype.playerBettingStateChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.bettingState = newValue;
};
PublicPokerGame.prototype.playerHighCardChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.highCard = newValue;
};
PublicPokerGame.prototype.playerStateChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.state = newValue;
};
PublicPokerGame.prototype.playerDisconnectedChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.disconnected = newValue;
};
PublicPokerGame.prototype.playerMarkedForRemovalChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.markedForRemoval = newValue;
};
PublicPokerGame.prototype.playerAwayStateChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.awayState = newValue;
};
PublicPokerGame.prototype.playerWinChancesChanged = function(seatIndex,oldValue,newValue) {
    this.seats[seatIndex].player.winChances = newValue;
};
PublicPokerGame.prototype.communityCardAdded = function(card) {
    this.communityCards.push(card);
};
PublicPokerGame.prototype.communityCardsAdded = function(cards) {
    this.communityCards = this.communityCards.concat(cards);
};
PublicPokerGame.prototype.communityCardRemoved = function(communityCardIndex) {
    this.communityCards.splice(communityCardIndex, 1)
};
PublicPokerGame.prototype.communityCardsCleared = function() {
    this.communityCards = [];
};
PublicPokerGame.prototype.dealerButtonChanged = function(oldValue,newValue) {
    this.dealerButton = newValue;
};
PublicPokerGame.prototype.stateChanged = function(oldValue,newValue) {
    this.state = newValue;
};
PublicPokerGame.prototype.potAdded = function(pot) {
    this.pots.push(pot);
};
PublicPokerGame.prototype.potsAdded = function(pots) {
    this.pots = this.pots.concat(pots);
};
PublicPokerGame.prototype.potRemoved = function(potIndex) {
    this.pots.splice(potIndex, 1)
};
PublicPokerGame.prototype.potsCleared = function() {
    this.pots = [];
};
PublicPokerGame.prototype.potPotAmountAdded = function(potIndex,potAmount) {
    this.pots[potIndex].potAmounts.push(potAmount);
};
PublicPokerGame.prototype.potPotAmountsAdded = function(potIndex,potAmounts) {
    this.pots[potIndex].potAmounts = this.pots[potIndex].potAmounts.concat(potAmounts);
};
PublicPokerGame.prototype.potPotAmountRemoved = function(potIndex,potAmountIndex) {
    this.pots[potIndex].potAmounts.splice(potAmountIndex, 1)
};
PublicPokerGame.prototype.potPotAmountsCleared = function(potIndex) {
    this.pots[potIndex].potAmounts = [];
};
PublicPokerGame.prototype.currentBetDecisionChanged = function(oldValue,newValue) {
    this.currentBetDecision = newValue;
};
PublicPokerGame.prototype.currentMuckDecisionChanged = function(oldValue,newValue) {
    this.currentMuckDecision = newValue;
};
PublicPokerGame.prototype.bigBlindSeatChanged = function(oldValue,newValue) {
    this.bigBlindSeat = newValue;
};
PublicPokerGame.prototype.bettingRoundChanged = function(oldValue,newValue) {
    this.bettingRound = newValue;
};
PublicPokerGame.prototype.resultChanged = function(oldValue,newValue) {
    this.result = newValue;
};
PublicPokerGame.prototype.chatMessageAdded = function(chatMessage) {
    this.chatMessages.push(chatMessage);
};
PublicPokerGame.prototype.chatMessagesAdded = function(chatMessages) {
    this.chatMessages = this.chatMessages.concat(chatMessages);
};
PublicPokerGame.prototype.chatMessageRemoved = function(chatMessageIndex) {
    this.chatMessages.splice(chatMessageIndex, 1)
};
PublicPokerGame.prototype.chatMessagesCleared = function() {
    this.chatMessages = [];
};
PublicPokerGame.prototype.currentMinimumBetChanged = function(oldValue,newValue) {
    this.currentMinimumBet = newValue;
};
PublicPokerGame.prototype.roundCounterChanged = function(oldValue,newValue) {
    this.roundCounter = newValue;
};
PublicPokerGame.prototype.markedForRemovalChanged = function(oldValue,newValue) {
    this.markedForRemoval = newValue;
};
PublicPokerGame.prototype.fixedBetSettingsChanged = function(oldValue,newValue) {
    this.fixedBetSettings = newValue;
};
//end PublicPokerGame
//start GameSettings
var GameSettings = function() {
    this.gameType    = null; // GameType    only LIMITED_DECK_PINEAPPLE is used in hov (TEXAS_HOLDEM,SEVEN_CARD_STUD,OMAHA_HI,OMAHA_HI_LO,FIVE_CARD_DRAW,CRAZY_PINEAPPLE,CHEAT,LIMITED_DECK_PINEAPPLE)
    this.bettingType = null; // BettingType only NO_LIMIT is used in hov (NO_LIMIT,FIXED_LIMIT,POT_LIMIT)
    this.tableName   = null; // String
    this.smallBet    = null; // Long        the blinds and minimum bet are derived from this
    this.ante        = null; // Long
    this.noOfSeats   = null; // Integer
    this.variant     = null; // Variant     n/a in hov (STRIPPED_DECK)
};
//end GameSettings
//start Seat
// models a seat. Is either empty or contains a player
var Seat = function() {
    this.player = null; // PublicPlayer
};
//end Seat
//start PrivatePlayer
var PrivatePlayer = function() {
    this.accountBalance = null; // Long               n/a in hov
    this.pocketCards    = null; // Card[]             a players hole cards ()
    this.allowedActions = null; // PokerAction[]      when it's this players turn, this contains their allowed actions. (means client doesn't have to figure this out) (BET,RAISE,CALL,CHECK,FOLD,INSURE)
    this.uiState        = null; // PrivatePlayerState not used in hov (OUT_OF_MONEY,IDLE,PLAYING,WANT_TO_PLAY_ASAP,WAITING_FOR_BIG_BLIND,WANT_TO_SIT_OUT,RESERVING,ONLOOKER)
    this.playerId       = null; // PlayerId
    this.mySeat         = null; // Integer            my seat index
    this.preTurnAction  = null; // PreTurnAction      n/a in hov (BET_ANY,RAISE_ANY,CALL_ANY,CALL,CHECK_FOLD,CHECK,FOLD)
};
PrivatePlayer.prototype.accountBalanceChanged = function(oldValue,newValue) {
    this.accountBalance = newValue;
};
PrivatePlayer.prototype.pocketCardAdded = function(card) {
    this.pocketCards.push(card);
};
PrivatePlayer.prototype.pocketCardsAdded = function(cards) {
    this.pocketCards = this.pocketCards.concat(cards);
};
PrivatePlayer.prototype.pocketCardRemoved = function(pocketCardIndex) {
    this.pocketCards.splice(pocketCardIndex, 1)
};
PrivatePlayer.prototype.pocketCardsCleared = function() {
    this.pocketCards = [];
};
PrivatePlayer.prototype.allowedActionAdded = function(pokerAction) {
    this.allowedActions.push(pokerAction);
};
PrivatePlayer.prototype.allowedActionsAdded = function(pokerActions) {
    this.allowedActions = this.allowedActions.concat(pokerActions);
};
PrivatePlayer.prototype.allowedActionRemoved = function(allowedActionIndex) {
    this.allowedActions.splice(allowedActionIndex, 1)
};
PrivatePlayer.prototype.allowedActionsCleared = function() {
    this.allowedActions = [];
};
PrivatePlayer.prototype.uiStateChanged = function(oldValue,newValue) {
    this.uiState = newValue;
};
PrivatePlayer.prototype.mySeatChanged = function(oldValue,newValue) {
    this.mySeat = newValue;
};
PrivatePlayer.prototype.preTurnActionChanged = function(oldValue,newValue) {
    this.preTurnAction = newValue;
};
//end PrivatePlayer
//start LimitedDeckExtension
var LimitedDeckExtension = function() {
    this.players             = null; // LimitedDeckPlayer[] maps players to their public limited deck state
    this.config              = null; // LimitedDeckConfig   all the fixed settings that are configurable by the server
    this.latestCheckpoint    = null; // Checkpoint          tracks the latest checkpoint.. game changes slightly at each checkpoint - a bit like levels in regular poker (NONE,_6_HANDS_PLAYED,_18_HANDS_PLAYED,SUDDEN_DEATH,SUDDEN_DEATH_OVERTIME,ELIMINATION_ROUND)
    this.roundCount          = null; // Integer             tracks round count since start of match
    this.lastCallClockPlayer = null; // PlayerId
    this.latestAbilityEvent  = null; // AbilityEvent
    this.latestBonusEvent    = null; // BonusEvent
    this.bonusCards          = null; // BonusCard[]         n/a in hov
};
LimitedDeckExtension.prototype.playerAdded = function(playerKey,limitedDeckPlayer) {
    this.players[playerKey] = limitedDeckPlayer;
};
LimitedDeckExtension.prototype.playerRemoved = function(playerKey) {
    delete this.players[playerKey];
};
LimitedDeckExtension.prototype.playersCleared = function() {
    this.players = {};
};
LimitedDeckExtension.prototype.abilitieCostChanged = function(playerKey,abilitieIndex,oldValue,newValue) {
    this.players[playerKey].config.abilities[abilitieIndex].cost = newValue;
};
LimitedDeckExtension.prototype.edgeCostChanged = function(playerKey,edgeIndex,oldValue,newValue) {
    this.players[playerKey].config.edges[edgeIndex].cost = newValue;
};
LimitedDeckExtension.prototype.playerPeekChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].peek = newValue;
};
LimitedDeckExtension.prototype.playerShownCardAdded = function(playerKey,card) {
    this.players[playerKey].shownCards.push(card);
};
LimitedDeckExtension.prototype.playerShownCardsAdded = function(playerKey,cards) {
    this.players[playerKey].shownCards = this.players[playerKey].shownCards.concat(cards);
};
LimitedDeckExtension.prototype.playerShownCardsCleared = function(playerKey) {
    this.players[playerKey].shownCards = [];
};
LimitedDeckExtension.prototype.playerPeekedCardAdded = function(playerKey,card) {
    this.players[playerKey].peekedCards.push(card);
};
LimitedDeckExtension.prototype.playerPeekedCardsAdded = function(playerKey,cards) {
    this.players[playerKey].peekedCards = this.players[playerKey].peekedCards.concat(cards);
};
LimitedDeckExtension.prototype.playerPeekedCardRemoved = function(playerKey,peekedCardIndex) {
    this.players[playerKey].peekedCards.splice(peekedCardIndex, 1)
};
LimitedDeckExtension.prototype.playerPeekedCardsCleared = function(playerKey) {
    this.players[playerKey].peekedCards = [];
};
LimitedDeckExtension.prototype.playerHandsWonChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].handsWon = newValue;
};
LimitedDeckExtension.prototype.playerFinalRankChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].finalRank = newValue;
};
LimitedDeckExtension.prototype.playerShowOneChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].showOne = newValue;
};
LimitedDeckExtension.prototype.playerUncontestedHandsWonChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].uncontestedHandsWon = newValue;
};
LimitedDeckExtension.prototype.playerBlockedChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].blocked = newValue;
};
LimitedDeckExtension.prototype.playerDeckStrengthChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].deckStrength = newValue;
};
LimitedDeckExtension.prototype.playerResourceAdded = function(playerKey,resource) {
    this.players[playerKey].resources.push(resource);
};
LimitedDeckExtension.prototype.playerResourcesAdded = function(playerKey,resources) {
    this.players[playerKey].resources = this.players[playerKey].resources.concat(resources);
};
LimitedDeckExtension.prototype.playerResourceRemoved = function(playerKey,resourceIndex) {
    this.players[playerKey].resources.splice(resourceIndex, 1)
};
LimitedDeckExtension.prototype.playerResourcesCleared = function(playerKey) {
    this.players[playerKey].resources = [];
};
LimitedDeckExtension.prototype.playerAchievementAdded = function(playerKey,achievement) {
    this.players[playerKey].achievements.push(achievement);
};
LimitedDeckExtension.prototype.playerAchievementsAdded = function(playerKey,achievements) {
    this.players[playerKey].achievements = this.players[playerKey].achievements.concat(achievements);
};
LimitedDeckExtension.prototype.playerAchievementsCleared = function(playerKey) {
    this.players[playerKey].achievements = [];
};
LimitedDeckExtension.prototype.playerSharkAwardAdded = function(playerKey,sharkAward) {
    this.players[playerKey].sharkAwards.push(sharkAward);
};
LimitedDeckExtension.prototype.playerSharkAwardsAdded = function(playerKey,sharkAwards) {
    this.players[playerKey].sharkAwards = this.players[playerKey].sharkAwards.concat(sharkAwards);
};
LimitedDeckExtension.prototype.playerSharkAwardRemoved = function(playerKey,sharkAwardIndex) {
    this.players[playerKey].sharkAwards.splice(sharkAwardIndex, 1)
};
LimitedDeckExtension.prototype.playerSharkAwardsCleared = function(playerKey) {
    this.players[playerKey].sharkAwards = [];
};
LimitedDeckExtension.prototype.playerPointsChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].points = newValue;
};
LimitedDeckExtension.prototype.playerCunningChanged = function(playerKey,oldValue,newValue) {
    this.players[playerKey].cunning = newValue;
};
LimitedDeckExtension.prototype.configRebuyAmountChanged = function(oldValue,newValue) {
    this.config.rebuyAmount = newValue;
};
LimitedDeckExtension.prototype.configPhoenixAmountChanged = function(oldValue,newValue) {
    this.config.phoenixAmount = newValue;
};
LimitedDeckExtension.prototype.configTopUpAmountChanged = function(oldValue,newValue) {
    this.config.topUpAmount = newValue;
};
LimitedDeckExtension.prototype.latestCheckpointChanged = function(oldValue,newValue) {
    this.latestCheckpoint = newValue;
};
LimitedDeckExtension.prototype.roundCountChanged = function(oldValue,newValue) {
    this.roundCount = newValue;
};
LimitedDeckExtension.prototype.lastCallClockPlayerChanged = function(oldValue,newValue) {
    this.lastCallClockPlayer = newValue;
};
LimitedDeckExtension.prototype.latestAbilityEventChanged = function(oldValue,newValue) {
    this.latestAbilityEvent = newValue;
};
LimitedDeckExtension.prototype.latestBonusEventChanged = function(oldValue,newValue) {
    this.latestBonusEvent = newValue;
};
LimitedDeckExtension.prototype.bonusCardAdded = function(bonusCard) {
    this.bonusCards.push(bonusCard);
};
LimitedDeckExtension.prototype.bonusCardsAdded = function(bonusCards) {
    this.bonusCards = this.bonusCards.concat(bonusCards);
};
LimitedDeckExtension.prototype.bonusCardRemoved = function(bonusCardIndex) {
    this.bonusCards.splice(bonusCardIndex, 1)
};
LimitedDeckExtension.prototype.bonusCardsCleared = function() {
    this.bonusCards = [];
};
//end LimitedDeckExtension
//start LimitedDeckConfig
var LimitedDeckConfig = function() {
    this.stackedPoints                 = null; // Integer
    this.stackedBlindsToReach          = null; // Integer
    this.firstBloodPoints              = null; // Integer
    this.stayingAlivePoints            = null; // Integer
    this.stayingAliveHandToSurvive     = null; // Integer
    this.doubleTroublePoints           = null; // Integer
    this.bustoPoints                   = null; // Integer
    this.bustoBottomPositionCount      = null; // Integer
    this.perfectPerfectPoints          = null; // Integer
    this.perfectPerfectMinPlayers      = null; // Integer
    this.winPointsMultiplier           = null; // Float
    this.epicWinPoints                 = null; // Integer
    this.acesDownPoints                = null; // Integer
    this.runnerUpPointsMultiplier      = null; // Float
    this.runnerUpMinPlayers            = null; // Integer
    this.epicLossPoints                = null; // Integer
    this.offToTheRacesPoints           = null; // Integer
    this.notAFlukePoints               = null; // Integer
    this.notAFlukeHandWonCount         = null; // Integer
    this.crushingPoints                = null; // Integer
    this.coinFlipPoints                = null; // Integer
    this.dominatedPoints               = null; // Integer
    this.gotchaPoints                  = null; // Integer
    this.smoothSailingHandWonCount     = null; // Integer
    this.smoothSailingPoints           = null; // Integer
    this.inControlHandWonCount         = null; // Integer
    this.inControlPoints               = null; // Integer
    this.doubleUpPoints                = null; // Integer
    this.mindTrickChips                = null; // Integer
    this.mindTrickPoints               = null; // Integer
    this.soulReadCards                 = null; // Integer
    this.readAndWeepPoints             = null; // Integer
    this.showboatingPoints             = null; // Integer
    this.heroFoldPoints                = null; // Integer
    this.heroCallPoints                = null; // Integer
    this.downToTwoPoints               = null; // Integer
    this.dodgedBulletPoints            = null; // Integer
    this.cruiseToVictoryStackThreshold = null; // Integer
    this.outplayedPointThreshold       = null; // Integer
    this.heroFoldMinPot                = null; // Integer
    this.heroFoldChanceDiffThreshold   = null; // Integer
    this.awayRoundLimit                = null; // Integer
    this.runForTheHillsPoints          = null; // Integer
    this.betDecisionTime               = null; // Long
    this.betDecisionShortTime          = null; // Long
    this.inTheDriversSeatPoints        = null; // Integer
    this.rebuyAmount                   = null; // Integer
    this.phoenixAmount                 = null; // Integer
    this.topUpAmount                   = null; // Integer
    this.smallStreakWinCount           = null; // Integer
    this.largeStreakWinCount           = null; // Integer
    this.streakCunningStealAmount      = null; // Integer
    this.streakPointsStealAmount       = null; // Integer
    this.pineappleCards                = null; // Integer
};
//end LimitedDeckConfig
//start PrivateLimitedDeckState
// hov state just for this player
var PrivateLimitedDeckState = function() {
    this.playerInfos = null; // PlayerInfo[] tracks players this player has collected about other players
    this.usedCards   = null; // Card[]       tracks cards from my limited deck that i have used so far ()
    this.result      = null; // SngResult    complex state describing a players results at end of game, all points and awards won etc
};
PrivateLimitedDeckState.prototype.playerInfoAdded = function(playerInfoKey,playerInfo) {
    this.playerInfos[playerInfoKey] = playerInfo;
};
PrivateLimitedDeckState.prototype.playerInfoRemoved = function(playerInfoKey) {
    delete this.playerInfos[playerInfoKey];
};
PrivateLimitedDeckState.prototype.playerInfosCleared = function() {
    this.playerInfos = {};
};
PrivateLimitedDeckState.prototype.playerInfoPeekedCardAdded = function(playerInfoKey,card) {
    this.playerInfos[playerInfoKey].peekedCards.push(card);
};
PrivateLimitedDeckState.prototype.playerInfoPeekedCardsAdded = function(playerInfoKey,cards) {
    this.playerInfos[playerInfoKey].peekedCards = this.playerInfos[playerInfoKey].peekedCards.concat(cards);
};
PrivateLimitedDeckState.prototype.playerInfoPeekedCardRemoved = function(playerInfoKey,peekedCardIndex) {
    this.playerInfos[playerInfoKey].peekedCards.splice(peekedCardIndex, 1)
};
PrivateLimitedDeckState.prototype.playerInfoPeekedCardsCleared = function(playerInfoKey) {
    this.playerInfos[playerInfoKey].peekedCards = [];
};
PrivateLimitedDeckState.prototype.playerInfoShownCardAdded = function(playerInfoKey,card) {
    this.playerInfos[playerInfoKey].shownCards.push(card);
};
PrivateLimitedDeckState.prototype.playerInfoShownCardsAdded = function(playerInfoKey,cards) {
    this.playerInfos[playerInfoKey].shownCards = this.playerInfos[playerInfoKey].shownCards.concat(cards);
};
PrivateLimitedDeckState.prototype.playerInfoShownCardRemoved = function(playerInfoKey,shownCardIndex) {
    this.playerInfos[playerInfoKey].shownCards.splice(shownCardIndex, 1)
};
PrivateLimitedDeckState.prototype.playerInfoShownCardsCleared = function(playerInfoKey) {
    this.playerInfos[playerInfoKey].shownCards = [];
};
PrivateLimitedDeckState.prototype.usedCardAdded = function(card) {
    this.usedCards.push(card);
};
PrivateLimitedDeckState.prototype.usedCardsAdded = function(cards) {
    this.usedCards = this.usedCards.concat(cards);
};
PrivateLimitedDeckState.prototype.usedCardRemoved = function(usedCardIndex) {
    this.usedCards.splice(usedCardIndex, 1)
};
PrivateLimitedDeckState.prototype.usedCardsCleared = function() {
    this.usedCards = [];
};
PrivateLimitedDeckState.prototype.resultChanged = function(oldValue,newValue) {
    this.result = newValue;
};
//end PrivateLimitedDeckState
//start PlayerInfo
var PlayerInfo = function() {
    this.peekedCards = null; // Card[] tracks another players cards which I've peeked privately (eye in sky) or others have exposed ()
    this.shownCards  = null; // Card[] tracks cards of other players shown in showdowns. This is crucial info in hov ()
};
//end PlayerInfo
//start SngResult
var SngResult = function() {
    this.finalRank        = null; // Integer       final rank in this hov sng
    this.cunning          = null; // Float         final cunning score
    this.sharkAwards      = null; // SharkAward[]  all shark awards won in the game
    this.sharkAwardPoints = null; // Integer       total points won from shark awards
    this.totalPoints      = null; // Float         net total points won. Zero is minimum
    this.pointsSpent      = null; // Float         points spent during the match. Affects total score
    this.achievements     = null; // Achievement[] achievements won during the game (CHIP_AND_CHAIR,CRUISE_TO_VICTORY,POCKETS_GALORE,RED_HANDED,OUTPLAYED,WILD_THING,CLEAN_SWEEP,PERFECT_READS)
    this.deduction        = null; // Float         deduction from total points derived from cunning spent etc
    this.boughtCunning    = null; // Float         cunning bought through use of certain abilities (mind trick)
    this.finished         = null; // Boolean       did the tournament end when elimination occurred
    this.pointsSubtotal   = null; // Float
};
//end SngResult
//start PlayerLocation
// describes a channel user is connected to. Sent to client when they connect, so they can join relevant channels
var PlayerLocation = function() {
    this.key     = null; // String
    this.appType = null; // AppType  (SNG,TOUR,CASH_GAME,TOUR_TABLE,LOBBY,CHAT,MULTI_TOUR,FORUM_THREAD,SINGLE_TABLE_TOUR,NEWSFEED,LOUNGE)
};
//end PlayerLocation
//start UserEntity
// n/a in hov
var UserEntity = function() {
    this.playerId            = null; // PlayerId
    this.userInfo            = null; // UserInfo
    this.accounts            = null; // Account[]
    this.userspaceLocation   = null; // Coord
    this.lastImageUploadTime = null; // Long
    this.lastLoginTime       = null; // Long
    this.status              = null; // String
    this.followedUsers       = null; // PlayerId[]
    this.playerPoints        = null; // Long
    this.enabledCharacters   = null; // PokerCharacter[]  (NONE,CAT,BIG_STACK,GENERAL,SHUFFLER,GUNG_HO,FINGERS,WUNDERKIND,HOLLYWOOD,TIGER_LILY)
};
UserEntity.prototype.userInfoPasswordChanged = function(oldValue,newValue) {
    this.userInfo.password = newValue;
};
UserEntity.prototype.accountBalanceChanged = function(accountIndex,oldValue,newValue) {
    this.accounts[accountIndex].balance = newValue;
};
UserEntity.prototype.lastImageUploadTimeChanged = function(oldValue,newValue) {
    this.lastImageUploadTime = newValue;
};
UserEntity.prototype.lastLoginTimeChanged = function(oldValue,newValue) {
    this.lastLoginTime = newValue;
};
UserEntity.prototype.statusChanged = function(oldValue,newValue) {
    this.status = newValue;
};
UserEntity.prototype.followedUserAdded = function(playerId) {
    this.followedUsers.push(playerId);
};
UserEntity.prototype.followedUsersAdded = function(playerIds) {
    this.followedUsers = this.followedUsers.concat(playerIds);
};
UserEntity.prototype.followedUserRemoved = function(followedUserIndex) {
    this.followedUsers.splice(followedUserIndex, 1)
};
UserEntity.prototype.followedUsersCleared = function() {
    this.followedUsers = [];
};
UserEntity.prototype.playerPointsChanged = function(oldValue,newValue) {
    this.playerPoints = newValue;
};
UserEntity.prototype.enabledCharacterAdded = function(pokerCharacter) {
    this.enabledCharacters.push(pokerCharacter);
};
UserEntity.prototype.enabledCharactersAdded = function(pokerCharacters) {
    this.enabledCharacters = this.enabledCharacters.concat(pokerCharacters);
};
UserEntity.prototype.enabledCharacterRemoved = function(enabledCharacterIndex) {
    this.enabledCharacters.splice(enabledCharacterIndex, 1)
};
UserEntity.prototype.enabledCharactersCleared = function() {
    this.enabledCharacters = [];
};
//end UserEntity
//start Account
var Account = function() {
    this.accountType = null; // AccountType
    this.balance     = null; // Long
};
//end Account
//start Coord
var Coord = function() {
    this.x = null; // Integer
    this.y = null; // Integer
};
//end Coord
//start Sng
// manages a waiting list to play on demand tournaments
var Sng = function() {
    this.key      = null; // String
    this.settings = null; // TourSettings static settings
    this.entrants = null; // Entrant[]    registered players, will typically kick of a new tournament every time a player limit is reached
    this.spawning = null; // Boolean      set very briefly while a new tournament is spawning. State may last milliseconds only.
};
Sng.prototype.entrantAdded = function(entrantKey,entrant) {
    this.entrants[entrantKey] = entrant;
};
Sng.prototype.entrantRemoved = function(entrantKey) {
    delete this.entrants[entrantKey];
};
Sng.prototype.entrantsCleared = function() {
    this.entrants = {};
};
Sng.prototype.entrantRegisterStateChanged = function(entrantKey,oldValue,newValue) {
    this.entrants[entrantKey].registerState = newValue;
};
Sng.prototype.spawningChanged = function(oldValue,newValue) {
    this.spawning = newValue;
};
//end Sng
//start Lounge
// a special sng waiting list whereby tournaments are spawned manually by the lounge manager rather than automatically. Also has chat.
var Lounge = function() {
    this.chatMessages = null; // ChatMessage[]
};
Lounge.prototype.chatMessageAdded = function(chatMessage) {
    this.chatMessages.push(chatMessage);
};
Lounge.prototype.chatMessagesAdded = function(chatMessages) {
    this.chatMessages = this.chatMessages.concat(chatMessages);
};
Lounge.prototype.chatMessageRemoved = function(chatMessageIndex) {
    this.chatMessages.splice(chatMessageIndex, 1)
};
Lounge.prototype.chatMessagesCleared = function() {
    this.chatMessages = [];
};
//end Lounge
//start TourClient
var TourClient = function() {
};
TourClient.prototype.setGameState = function(principal, publicState, privateState) {}; // sent to client when the channel is joined
//end TourClient
//start PublicTourListener
var PublicTourListener = function() {
};
PublicTourListener.prototype.registeringStateChanged = function(oldValue, newValue) {};
PublicTourListener.prototype.registeringStateEntrantAdded = function(entrant) {};
PublicTourListener.prototype.registeringStateEntrantsAdded = function(entrants) {};
PublicTourListener.prototype.registeringStateEntrantRemoved = function(entrantIndex, removed) {};
PublicTourListener.prototype.registeringStateEntrantsCleared = function() {};
PublicTourListener.prototype.entrantRegisterStateChanged = function(entrantIndex, oldValue, newValue) {};
PublicTourListener.prototype.runningStateChanged = function(oldValue, newValue) {};
PublicTourListener.prototype.playerStackChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerFinalRankChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerPrizeChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerEliminatedChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerMoveCountChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerMarkedForMoveChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerRankChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.playerQuitChanged = function(playerIndex, oldValue, newValue) {};
PublicTourListener.prototype.runningStateStateChanged = function(oldValue, newValue) {};
PublicTourListener.prototype.tableSeatAdded = function(tableIndex, tSeat) {};
PublicTourListener.prototype.tableSeatsAdded = function(tableIndex, tSeats) {};
PublicTourListener.prototype.tableSeatRemoved = function(tableIndex, seatIndex, removed) {};
PublicTourListener.prototype.tableSeatsCleared = function(tableIndex) {};
PublicTourListener.prototype.seatTourPlayerIdChanged = function(tableIndex, seatIndex, oldValue, newValue) {};
PublicTourListener.prototype.tableIdleChanged = function(tableIndex, oldValue, newValue) {};
PublicTourListener.prototype.tableRemovedChanged = function(tableIndex, oldValue, newValue) {};
PublicTourListener.prototype.runningStateCurrentLevelChanged = function(oldValue, newValue) {};
PublicTourListener.prototype.runningStatePauseTimestampChanged = function(oldValue, newValue) {};
PublicTourListener.prototype.stateChanged = function(oldValue, newValue) {};
PublicTourListener.prototype.externalTourKeyChanged = function(oldValue, newValue) {};
//end PublicTourListener
//start PrivateTourStateListener
var PrivateTourStateListener = function() {
};
PrivateTourStateListener.prototype.privateRunningStateChanged = function(oldValue, newValue) {};
PrivateTourStateListener.prototype.privateRunningStateMyTableKeyChanged = function(oldValue, newValue) {};
PrivateTourStateListener.prototype.privateRunningStateMyRankChanged = function(oldValue, newValue) {};
PrivateTourStateListener.prototype.privateRunningStateMyStackChanged = function(oldValue, newValue) {};
PrivateTourStateListener.prototype.privateRunningStateMyTourPlayerIdChanged = function(oldValue, newValue) {};
PrivateTourStateListener.prototype.registeredChanged = function(oldValue, newValue) {};
//end PrivateTourStateListener
//start PrivatePlayerListener
var PrivatePlayerListener = function() {
};
PrivatePlayerListener.prototype.accountBalanceChanged = function(oldValue, newValue) {};
PrivatePlayerListener.prototype.pocketCardAdded = function(card) {};
PrivatePlayerListener.prototype.pocketCardsAdded = function(cards) {};
PrivatePlayerListener.prototype.pocketCardRemoved = function(pocketCardIndex, removed) {};
PrivatePlayerListener.prototype.pocketCardsCleared = function() {};
PrivatePlayerListener.prototype.allowedActionAdded = function(pokerAction) {};
PrivatePlayerListener.prototype.allowedActionsAdded = function(pokerActions) {};
PrivatePlayerListener.prototype.allowedActionRemoved = function(allowedActionIndex, removed) {};
PrivatePlayerListener.prototype.allowedActionsCleared = function() {};
PrivatePlayerListener.prototype.uiStateChanged = function(oldValue, newValue) {};
PrivatePlayerListener.prototype.mySeatChanged = function(oldValue, newValue) {};
PrivatePlayerListener.prototype.preTurnActionChanged = function(oldValue, newValue) {};
//end PrivatePlayerListener
//start PublicPokerGameListener
var PublicPokerGameListener = function() {
};
PublicPokerGameListener.prototype.seatPlayerChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerUsernameChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerStackChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerVisibleCardAdded = function(seatIndex, card) {};
PublicPokerGameListener.prototype.playerVisibleCardsAdded = function(seatIndex, cards) {};
PublicPokerGameListener.prototype.playerVisibleCardRemoved = function(seatIndex, visibleCardIndex, removed) {};
PublicPokerGameListener.prototype.playerVisibleCardsCleared = function(seatIndex) {};
PublicPokerGameListener.prototype.playerAccountBalanceChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerCurrentBetChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerNoHiddenCardsChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerLatestPokerActionChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerBettingStateChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerHighCardChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerStateChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerDisconnectedChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerMarkedForRemovalChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerAwayStateChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.playerWinChancesChanged = function(seatIndex, oldValue, newValue) {};
PublicPokerGameListener.prototype.communityCardAdded = function(card) {};
PublicPokerGameListener.prototype.communityCardsAdded = function(cards) {};
PublicPokerGameListener.prototype.communityCardRemoved = function(communityCardIndex, removed) {};
PublicPokerGameListener.prototype.communityCardsCleared = function() {};
PublicPokerGameListener.prototype.dealerButtonChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.stateChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.potAdded = function(pot) {};
PublicPokerGameListener.prototype.potsAdded = function(pots) {};
PublicPokerGameListener.prototype.potRemoved = function(potIndex, removed) {};
PublicPokerGameListener.prototype.potsCleared = function() {};
PublicPokerGameListener.prototype.potPotAmountAdded = function(potIndex, potAmount) {};
PublicPokerGameListener.prototype.potPotAmountsAdded = function(potIndex, potAmounts) {};
PublicPokerGameListener.prototype.potPotAmountRemoved = function(potIndex, potAmountIndex, removed) {};
PublicPokerGameListener.prototype.potPotAmountsCleared = function(potIndex) {};
PublicPokerGameListener.prototype.currentBetDecisionChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.currentMuckDecisionChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.bigBlindSeatChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.bettingRoundChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.resultChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.chatMessageAdded = function(chatMessage) {};
PublicPokerGameListener.prototype.chatMessagesAdded = function(chatMessages) {};
PublicPokerGameListener.prototype.chatMessageRemoved = function(chatMessageIndex, removed) {};
PublicPokerGameListener.prototype.chatMessagesCleared = function() {};
PublicPokerGameListener.prototype.currentMinimumBetChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.roundCounterChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.markedForRemovalChanged = function(oldValue, newValue) {};
PublicPokerGameListener.prototype.fixedBetSettingsChanged = function(oldValue, newValue) {};
//end PublicPokerGameListener
//start LimitedDeckExtensionListener
var LimitedDeckExtensionListener = function() {
};
LimitedDeckExtensionListener.prototype.playerAdded = function(playerKey, limitedDeckPlayer) {};
LimitedDeckExtensionListener.prototype.playerRemoved = function(playerKey, removed) {};
LimitedDeckExtensionListener.prototype.playersCleared = function() {};
LimitedDeckExtensionListener.prototype.abilitieCostChanged = function(playerKey, abilitieIndex, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.edgeCostChanged = function(playerKey, edgeIndex, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerPeekChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerShownCardAdded = function(playerKey, card) {};
LimitedDeckExtensionListener.prototype.playerShownCardsAdded = function(playerKey, cards) {};
LimitedDeckExtensionListener.prototype.playerShownCardsCleared = function(playerKey) {};
LimitedDeckExtensionListener.prototype.playerPeekedCardAdded = function(playerKey, card) {};
LimitedDeckExtensionListener.prototype.playerPeekedCardsAdded = function(playerKey, cards) {};
LimitedDeckExtensionListener.prototype.playerPeekedCardRemoved = function(playerKey, peekedCardIndex, removed) {};
LimitedDeckExtensionListener.prototype.playerPeekedCardsCleared = function(playerKey) {};
LimitedDeckExtensionListener.prototype.playerHandsWonChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerFinalRankChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerShowOneChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerUncontestedHandsWonChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerBlockedChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerDeckStrengthChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerResourceAdded = function(playerKey, resource) {};
LimitedDeckExtensionListener.prototype.playerResourcesAdded = function(playerKey, resources) {};
LimitedDeckExtensionListener.prototype.playerResourceRemoved = function(playerKey, resourceIndex, removed) {};
LimitedDeckExtensionListener.prototype.playerResourcesCleared = function(playerKey) {};
LimitedDeckExtensionListener.prototype.playerAchievementAdded = function(playerKey, achievement) {};
LimitedDeckExtensionListener.prototype.playerAchievementsAdded = function(playerKey, achievements) {};
LimitedDeckExtensionListener.prototype.playerAchievementsCleared = function(playerKey) {};
LimitedDeckExtensionListener.prototype.playerSharkAwardAdded = function(playerKey, sharkAward) {};
LimitedDeckExtensionListener.prototype.playerSharkAwardsAdded = function(playerKey, sharkAwards) {};
LimitedDeckExtensionListener.prototype.playerSharkAwardRemoved = function(playerKey, sharkAwardIndex, removed) {};
LimitedDeckExtensionListener.prototype.playerSharkAwardsCleared = function(playerKey) {};
LimitedDeckExtensionListener.prototype.playerPointsChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.playerCunningChanged = function(playerKey, oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.configRebuyAmountChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.configPhoenixAmountChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.configTopUpAmountChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.latestCheckpointChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.roundCountChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.lastCallClockPlayerChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.latestAbilityEventChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.latestBonusEventChanged = function(oldValue, newValue) {};
LimitedDeckExtensionListener.prototype.bonusCardAdded = function(bonusCard) {};
LimitedDeckExtensionListener.prototype.bonusCardsAdded = function(bonusCards) {};
LimitedDeckExtensionListener.prototype.bonusCardRemoved = function(bonusCardIndex, removed) {};
LimitedDeckExtensionListener.prototype.bonusCardsCleared = function() {};
//end LimitedDeckExtensionListener
//start PokerGameClient
var PokerGameClient = function() {
};
PokerGameClient.prototype.setGameState = function(principal, publicState, privateState) {};
//end PokerGameClient
//start LimitedDeckServiceClient
var LimitedDeckServiceClient = function() {
};
LimitedDeckServiceClient.prototype.setExtensionState = function(principal, state, privateState) {};
LimitedDeckServiceClient.prototype.errorMessage = function(principal, errorCode) {};
LimitedDeckServiceClient.prototype.mechanicsGripInfo = function(principal, cardsRemoved, cardsReturned) {}; // special message to inform the client about the deck changes of mechanics grip
//end LimitedDeckServiceClient
//start PrivateLimitedDeckStateListener
var PrivateLimitedDeckStateListener = function() {
};
PrivateLimitedDeckStateListener.prototype.playerInfoAdded = function(playerInfoKey, playerInfo) {};
PrivateLimitedDeckStateListener.prototype.playerInfoRemoved = function(playerInfoKey, removed) {};
PrivateLimitedDeckStateListener.prototype.playerInfosCleared = function() {};
PrivateLimitedDeckStateListener.prototype.playerInfoPeekedCardAdded = function(playerInfoKey, card) {};
PrivateLimitedDeckStateListener.prototype.playerInfoPeekedCardsAdded = function(playerInfoKey, cards) {};
PrivateLimitedDeckStateListener.prototype.playerInfoPeekedCardRemoved = function(playerInfoKey, peekedCardIndex, removed) {};
PrivateLimitedDeckStateListener.prototype.playerInfoPeekedCardsCleared = function(playerInfoKey) {};
PrivateLimitedDeckStateListener.prototype.playerInfoShownCardAdded = function(playerInfoKey, card) {};
PrivateLimitedDeckStateListener.prototype.playerInfoShownCardsAdded = function(playerInfoKey, cards) {};
PrivateLimitedDeckStateListener.prototype.playerInfoShownCardRemoved = function(playerInfoKey, shownCardIndex, removed) {};
PrivateLimitedDeckStateListener.prototype.playerInfoShownCardsCleared = function(playerInfoKey) {};
PrivateLimitedDeckStateListener.prototype.usedCardAdded = function(card) {};
PrivateLimitedDeckStateListener.prototype.usedCardsAdded = function(cards) {};
PrivateLimitedDeckStateListener.prototype.usedCardRemoved = function(usedCardIndex, removed) {};
PrivateLimitedDeckStateListener.prototype.usedCardsCleared = function() {};
PrivateLimitedDeckStateListener.prototype.resultChanged = function(oldValue, newValue) {};
//end PrivateLimitedDeckStateListener
//start UserApiReply
var UserApiReply = function() {
};
UserApiReply.prototype.signUpResponse = function(principal, errorCode) {};
UserApiReply.prototype.loginResponse = function(principal, errorCode) {};
UserApiReply.prototype.loginAsGuestResponse = function(principal, errorCode) {};
UserApiReply.prototype.resetPasswordResponse = function(errorCode) {};
UserApiReply.prototype.loginWithTokenResponse = function(errorCode) {};
UserApiReply.prototype.loginWithPlayerSessionResponse = function(principal, sngKey, errorCode) {};
UserApiReply.prototype.startSinglePlayerGameResponse = function(principal, errorCode) {};
UserApiReply.prototype.loginAsObserverResponse = function(principal, errorCode) {};
//end UserApiReply
//start ClientControl
var ClientControl = function() {
};
ClientControl.prototype.setInitialInfo = function(principal, locations, serverTime, guest) {}; // called when a user logs in to tell client the apps that the player is associated with. i.e. what tables they are sitting at and what tournaments they are registered to
ClientControl.prototype.forceJoinChannel = function(principal, channelId, appType) {}; // used to direct the client to join a channel, for example when a sng spawns, the client should join the created tournament
ClientControl.prototype.setUser = function(principal, user) {}; // sets the high level user state on the client
ClientControl.prototype.ping = function(principal) {};
//end ClientControl
//start SngClient
var SngClient = function() {
};
SngClient.prototype.setSngState = function(principal, sngState) {};
//end SngClient
//start SngListener
var SngListener = function() {
};
SngListener.prototype.entrantAdded = function(entrantKey, entrant) {};
SngListener.prototype.entrantRemoved = function(entrantKey, removed) {};
SngListener.prototype.entrantsCleared = function() {};
SngListener.prototype.entrantRegisterStateChanged = function(entrantKey, oldValue, newValue) {};
SngListener.prototype.spawningChanged = function(oldValue, newValue) {};
//end SngListener
//start RegisterServiceReply
var RegisterServiceReply = function() {
};
RegisterServiceReply.prototype.registerResponse = function(principal, errorCode) {};
RegisterServiceReply.prototype.unregisterResponse = function(principal, errorCode) {};
RegisterServiceReply.prototype.prepareForRemovalResponse = function(principal, errorCode) {};
//end RegisterServiceReply
//start ChannelReply
var ChannelReply = function() {
};
ChannelReply.prototype.joinResponse = function(principal, errorCode) {};
ChannelReply.prototype.leaveResponse = function(principal, errorCode) {};
ChannelReply.prototype.playerConnectedResponse = function(principal, errorCode) {};
ChannelReply.prototype.playerDisconnectedResponse = function(principal, errorCode) {};
//end ChannelReply
//start LimitedDeckServiceReply
var LimitedDeckServiceReply = function() {
};
LimitedDeckServiceReply.prototype.peekHoleCardsResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.shuffleDeckResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.showOneResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.blockResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.swapResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.buyExtraLifeResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.topUpResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.mindTrickResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.soulReadResponse = function(principal, errorCode) {};
LimitedDeckServiceReply.prototype.undoResponse = function(principal, errorCode) {};
//end LimitedDeckServiceReply
//start LoungeClient
var LoungeClient = function() {
};
LoungeClient.prototype.setLoungeState = function(principal, loungeState, sngState) {};
LoungeClient.prototype.errorNotification = function(principal, errorCode, message) {};
//end LoungeClient
//start LoungeListener
var LoungeListener = function() {
};
LoungeListener.prototype.chatMessageAdded = function(chatMessage) {};
LoungeListener.prototype.chatMessagesAdded = function(chatMessages) {};
LoungeListener.prototype.chatMessageRemoved = function(chatMessageIndex, removed) {};
LoungeListener.prototype.chatMessagesCleared = function() {};
//end LoungeListener
//start LimitedDeckService
// Api for client to interact with the hov game at the table channel
var LimitedDeckService = function(serverProxy, key) {
    this.serverProxy = serverProxy;
    this.key = key
};
LimitedDeckService.prototype.peekHoleCards = function(player, publicPeek) {
    this.serverProxy.sendMessage(487, {
        key : this.key,
        principal : this.serverProxy.principal,
        player : player,
        publicPeek : publicPeek
    });
};
LimitedDeckService.prototype.shuffleDeck = function(player) {
    this.serverProxy.sendMessage(289, {
        key : this.key,
        principal : this.serverProxy.principal,
        player : player
    });
};
LimitedDeckService.prototype.showOne = function(playerId) {
    this.serverProxy.sendMessage(335, {
        key : this.key,
        principal : this.serverProxy.principal,
        playerId : playerId
    });
};
LimitedDeckService.prototype.block = function() {
    this.serverProxy.sendMessage(627, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
LimitedDeckService.prototype.swap = function(card) {
    this.serverProxy.sendMessage(647, {
        key : this.key,
        principal : this.serverProxy.principal,
        card : card
    });
};
LimitedDeckService.prototype.buyExtraLife = function() {
    this.serverProxy.sendMessage(658, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
LimitedDeckService.prototype.topUp = function() {
    this.serverProxy.sendMessage(655, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
LimitedDeckService.prototype.mindTrick = function() {
    this.serverProxy.sendMessage(283, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
LimitedDeckService.prototype.soulRead = function(playerId) { // Reveal six random cards in a player's muck pile.
    this.serverProxy.sendMessage(286, {
        key : this.key,
        principal : this.serverProxy.principal,
        playerId : playerId
    });
};
LimitedDeckService.prototype.undo = function(ability) {
    this.serverProxy.sendMessage(722, {
        key : this.key,
        principal : this.serverProxy.principal,
        ability : ability
    });
};
//end LimitedDeckService
//start PokerGamePlay
//
var PokerGamePlay = function(serverProxy, key) {
    this.serverProxy = serverProxy;
    this.key = key
};
PokerGamePlay.prototype.doPlayAction = function(action, betAmount) { // general poker play action, such as bet, raise, fold etc
    this.serverProxy.sendMessage(36, {
        key : this.key,
        principal : this.serverProxy.principal,
        action : action,
        betAmount : betAmount
    });
};
PokerGamePlay.prototype.showCardsDecision = function(showCard1, showCard2) { // allows players to show one or both of their cards
    this.serverProxy.sendMessage(37, {
        key : this.key,
        principal : this.serverProxy.principal,
        showCard1 : showCard1,
        showCard2 : showCard2
    });
};
PokerGamePlay.prototype.updatePreTurnAction = function(action) {
    this.serverProxy.sendMessage(38, {
        key : this.key,
        principal : this.serverProxy.principal,
        action : action
    });
};
PokerGamePlay.prototype.newChatMessage = function(message) {
    this.serverProxy.sendMessage(39, {
        key : this.key,
        principal : this.serverProxy.principal,
        message : message
    });
};
PokerGamePlay.prototype.iAmBack = function() {
    this.serverProxy.sendMessage(40, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
PokerGamePlay.prototype.selectCards = function(cards) { // allows the user to send a set of cards. Example of use is for the discard a card feature in pineapple games
    this.serverProxy.sendMessage(41, {
        key : this.key,
        principal : this.serverProxy.principal,
        cards : cards
    });
};
PokerGamePlay.prototype.requestExtraTime = function() {
    this.serverProxy.sendMessage(616, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
PokerGamePlay.prototype.callClock = function() { // Calls the clock on the current player
    this.serverProxy.sendMessage(639, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
PokerGamePlay.prototype.insure = function() { // allows player to keep checking even if the pot is raised
    this.serverProxy.sendMessage(712, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
PokerGamePlay.prototype.straddle = function() { // The player first to act next hand automatically raises to 2
    this.serverProxy.sendMessage(57, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
PokerGamePlay.prototype.togglePauseGame = function() { // pause the game in its current state
    this.serverProxy.sendMessage(526, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
//end PokerGamePlay
//start UserApi
//
var UserApi = function(serverProxy) {
    this.serverProxy = serverProxy;
};
UserApi.prototype.signUp = function(userInfo, profileImage) {
    this.serverProxy.sendMessage(61, {
        userInfo : userInfo,
        profileImage : profileImage
    });
};
UserApi.prototype.login = function(nickname, password) {
    this.serverProxy.sendMessage(62, {
        nickname : nickname,
        password : password
    });
};
UserApi.prototype.loginAsGuest = function(nickname) {
    this.serverProxy.sendMessage(63, {
        nickname : nickname
    });
};
UserApi.prototype.resetPassword = function(nickname, emailAddress) { // user enters email address to help prevent other users maliciously reseting passwords
    this.serverProxy.sendMessage(64, {
        nickname : nickname,
        emailAddress : emailAddress
    });
};
UserApi.prototype.loginWithToken = function(principal, token, bot, useCase) {
    this.serverProxy.sendMessage(610, {
        principal : principal,
        token : token,
        bot : bot,
        useCase : useCase
    });
};
UserApi.prototype.loginWithPlayerSession = function(playerSessionId, authToken, useCase) {
    this.serverProxy.sendMessage(618, {
        playerSessionId : playerSessionId,
        authToken : authToken,
        useCase : useCase
    });
};
UserApi.prototype.startSinglePlayerGame = function(authToken, userId, botCount, pokerCharacter, botDecisionTime) {
    this.serverProxy.sendMessage(640, {
        authToken : authToken,
        userId : userId,
        botCount : botCount,
        pokerCharacter : pokerCharacter,
        botDecisionTime : botDecisionTime
    });
};
UserApi.prototype.loginAsObserver = function(userId) {
    this.serverProxy.sendMessage(670, {
        userId : userId
    });
};
//end UserApi
//start Channel
//
var Channel = function(serverProxy, key) {
    this.serverProxy = serverProxy;
    this.key = key
};
Channel.prototype.join = function() { // a player wants to join a channel
    this.serverProxy.sendMessage(164, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
Channel.prototype.leave = function() { // A player wants to leave a channel
    this.serverProxy.sendMessage(165, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
Channel.prototype.playerConnected = function() {
    this.serverProxy.sendMessage(166, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
Channel.prototype.playerDisconnected = function() {
    this.serverProxy.sendMessage(167, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
//end Channel
//start RegisterService
//
var RegisterService = function(serverProxy, key) {
    this.serverProxy = serverProxy;
    this.key = key
};
RegisterService.prototype.register = function(ipAddress, hardwareAddresses, password, attachment) {
    this.serverProxy.sendMessage(103, {
        key : this.key,
        principal : this.serverProxy.principal,
        ipAddress : ipAddress,
        hardwareAddresses : hardwareAddresses,
        password : password,
        attachment : attachment
    });
};
RegisterService.prototype.unregister = function() {
    this.serverProxy.sendMessage(104, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
RegisterService.prototype.prepareForRemoval = function() {
    this.serverProxy.sendMessage(105, {
        key : this.key,
        principal : this.serverProxy.principal
    });
};
//end RegisterService
//start LoungeService
//
var LoungeService = function(serverProxy, key) {
    this.serverProxy = serverProxy;
    this.key = key
};
LoungeService.prototype.newChatMessage = function(message) {
    this.serverProxy.sendMessage(709, {
        key : this.key,
        principal : this.serverProxy.principal,
        message : message
    });
};
LoungeService.prototype.triggerSng = function(playerIds, name) { // kick off a tournament between n of the players in the lounge
    this.serverProxy.sendMessage(710, {
        key : this.key,
        principal : this.serverProxy.principal,
        playerIds : playerIds,
        name : name
    });
};
//end LoungeService