# Game Server times explained for Ending Hands

## Intro
During the game the GS sends messages to the client like `PublicPokerGame.setState`. This message contains a symbol representing the server's view of the current game state.
The state can have many values (see appendix below), but I'll just talk about ending hands here
```
ENDING_HAND or ALL_IN_SEQUENCE
SHOWDOWN
POST_ROUND
```
          
Each hand will EITHER end with an all-in sequence where at least one player will be eliminated, OR, it will end normally
                   
### "Normal"  ENDING_HAND
After the final Pot is collected, the GS moves to `ENDING_HAND` for `0.5 seconds` (configurable)

### ALL_IN_SEQUENCE
If all players are All-in, the GS will move to `ALL_IN_SEQUENCE`
The time span is calculated as follows:

The following constants are configurable (shown with current values):
```
allInSequenceFactor: 3 seconds
allInSequenceMinimumTime: 2 seconds
```
Then we have the following variables (depending on the hand in question):
```
number representing stage when everyone went all-in
pre-flop: 4
flop:     3
turn:     2
river:    1
let's call this number R
```
The all in sequence time is calculated like this:
```
T = allInSequenceMinimumTime + ( allInSequenceFactor x R)
```
so, if we're all-in pre-flop then the calculation would be
```
T = 2 + (4 x 3) = 14 seconds
```

The "extra time for hands of victory" is calculated as follows:
```
let S = maximum number of Shark Awards won during allin sequence (say player A wins 1 and player B wins 2 then S will be 2)
const baseTime = 0.5 seconds (configurable)
const factor = 0.5 seconds (configurable)
if S > 0 then the extra time will be (baseTime + (S x factor))

```

### SHOWDOWN
Note, that regardless of whether there actually is a showdown, the GS will move to the SHOWDOWN state.
The time span is calculated as follows:

The following constants are configurable (shown with current values):
```
payoutTimeFactor: 2 seconds
showdownTimeFactor: 2 seconds
showdownMinimum: 5 seconds
```
Then we have the following variables (depending on the hand in question):
```
numberOfPots
numberOfShownHands
```
The showdown time is calculated like this:                  
```
  T = (payoutTimeFactor x numberOfPots) + (showdownTimeFactor x numberOfShownHands)
  T = MAX(T, showdownMinimum) // this ensures the time will never be less than the configured minimum
  T = T + extra time for hands of victory
```
The "extra time for hands of victory" is calculated as follows:
```
let S = maximum number of Shark Awards won during showdown (say player A wins 1 and player B wins 2 then S will be 2)
const baseTime = 0.5 seconds (configurable)
const factor = 0.5 seconds (configurable)
if S > 0 then the extra time will be (baseTime + (S x factor))

```

### POST_ROUND
The post round time span `P` is calculated as follows:

1. Let's start with `P = 0`

2. if there are 1 or more players eliminated then add `2 seconds (configurable)` to `P` 

3. Add time to `P` for shark awards calculated as for `SHOWDOWN` above

4. Add time `1 second (configurable)` for each player if any of the following were fired `expose, topUp or mechanicsGrip` .


 

### Appendix
All the GameState values:
```
WAITING,
EXPECTING_MUCK_DECISION,
EXPECTING_BET_DECISION,
EXPECTING_CARD_SELECTION,
DEALING_CARDS,
EXPECTING_BLINDS_DECISION,
PAYING_BLINDS,
HIGH_CARD_FOR_BUTTON,
DEALING_FLOP,
DEALING_TURN,
DEALING_RIVER,
ENDING_HAND,
ALL_IN_SEQUENCE,
SHOWDOWN,
POST_BET_DECISION,
POST_BETTING_ROUND,
SHUFFLING_CARDS,
PAYING_ANTES,
POST_ROUND,
MORE_TIME_REQUESTED,
CALL_CLOCK_REQUESTED,
PAUSED,
CONTROLLED_BY_EXTENSION,
STOPPED_THE_CLOCK
```
