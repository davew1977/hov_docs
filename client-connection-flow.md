### Key Concepts:
#####Channel: 
The game client connects to various channels on the server. Each channel handles a distinct set of messages. For example there is a channel for each poker game, and a separate channel for each tournament. In HandsOfVictory, for each game, there is a poker game channel AND a tournament channel. There is an SNG channel handling updates for when the player is on the waiting list waiting for a game to start

#####Public/Private Game state
I’ll describe Public/PrivateGame - same principle applies to Tour too.
PublicGame contains all the publicly viewable game state for an ongoing game. If you just observe a game and don’t participate, this state is all you will see. PrivateState contains data  that only you should be able to see. Most obvious is the information about your pocket cards, but also things related to your UI state (like possible bet actions you can make) that the other players’ clients doesn’t need to visualise the game.

in case you hadn’t seen yet, you can see what’s in the private game state in the `model.generated.js` file:
```
    var PrivatePlayer = function() {
        this.accountBalance = null; // Long
        this.pocketCards = null; // Card
        this.allowedActions = null; // PokerAction
        this.uiState = null; // PrivatePlayerState
        this.playerId = null; // PlayerId
        this.mySeat = null; // Integer
        this.preTurnAction = null; // PreTurnAction
    };
```

###Client Connection Flow
Here we describe how the web table client goes from page load to being connected to the poker game

1. `hov.php` is loaded, which loads all the js files. Some constants are set up from the php context, 
   - such as whether we are in single player mode
   - the user id
   - the server to connect to

1. the initial server connect happens in `hov-client.js`. The websocket is opened, and a callback is provided. One of three modes will be chosen:
    
    - `loginAsObserver` This allows anyone to watch a game
    - `startSinglePlayerGame` This allows a single player practice game
    - `loginWithPlayerSession` This is the "normal" multi-player route, and authtoken is required
    
1. Once connected, the client listens for one the the following messages from the server (see `hov-client.js`)    
   
   - `clientControl.forceJoinChannel` this is when a game starts when the client is connected
   - `clientControl.setInitialInfo` this will be called every time, and handles the case when the client connects during a game session, which is very common especially on the web - for example every time they refresh the page
   
1. `clientControl.setInitialInfo` sends info to the client about what channels they are connected to, and is an instruction to join them immediately. If a game is already underway, the client will be instructed to first join the tournament channel

1. Once a channel is joined, the first thing the server counterpart will send is a snapshot of the channel's state. In `hov-client.js` you see several functions like `tourClient.setGameState`, `pokerGameClient.setGameState `, `limitedDeckServiceClient.setExtensionState`. These functions handle the receiving of these initial states. They will set up a local state object which will be set up to automatically track the server updates as they stream down the channel.

1. in `tourClient.setGameState`, after the bookkeeping described above, and if the tournament is in the `RUNNING` state, then the table itself will be joined. (background: the poker engine was initially designed to handle tournaments with multiple simultaneous tables, which explains why the tournament and poker game channels are separate. In HandsofVictory we only have single table tournaments)

1. When joining the table channel, the server sends 2 separate state objects for the client to track: the "PokerGame" and the "LimitedDeckExtension". The pokergame contains the classic poker game state. The `LimitedDeckExtension` contains all the state which makes HandsOfVictory different from a regular poker game

1. Once the table channel is joined, and the 2 state objects are set up, the table is initialised. `model2view.js::pokerGameLoaded` and then `model2view.js::extensionLoaded` are called. Here the UI is set up according to the game state received in the state above
      
